一、	简介	

二、	如何搭建

三、	主要的技术	

四、	其他主要插件
	
五、	package命令（项目从此开始）	

六、	webpack项目配置（版本为：webpack: 4.16.5）	

    一)	基本共享配置webpack.base.conf.js

    二)	开发版的配置 webpack.dev.conf.js: 方便开发所需做的额外配置

    三)	生产环境的 webpack.prod.conf.js 代码要求，浏览器能直接运行，简单，按需加载

    四)	对构建生产环境的扩展讲解： build/build.js	

七、	服务器启动代码src/copy/index.js	

八、	服务器管理PM2	

九、	异常的整合	

十、	项目的整体结构	

十一、	项目代码入口讲解main.js	

十二、	主要组件封装讲解	

    一)Ajax 封装 src/utils/ajax	

    二)	Auth 用户信息统一管理	

    三)	Easemob 聊天消息分封装	

    四)	Im 业务逻辑的封装，要和后端数据进行交互的部分
	
    五)	opermission 权限的封装src/permisssion	

    六)	StompJs封装讲解	

    七） BasicTable组件封装

    八) 埋点

十三、Git的项目管理

十四、小二商城

======================================================================

 **一、简介** 

1.oms是使用vue+webpack+node搭建的一套前后端分离的的前端系统。

2.整体项目是基于vue-element-admin这一套全后端模板样式进行封装。

3.里面集成了：权限管理、文本编辑器、excel导入导出、动态表格...

4.项目地址：http://192.168.10.168:10080/front/oms

5.基本支持ie9，ie9以下不兼容： ie10 以上才有良好的展示

 **二、如何搭建** 

1.预先安装好node 、git 和cnpm 环境（官网下下载，默认安装就新，建议下长期版本的）

2.git clone https://github.com/PanJiaChen/vue-element-admin.git

3.进入目录 cnpm install

4.进入开发模式 npm run dev

5.编译成为线上发布版本 npm run build

 **三、主要的技术** 

1.webpack: 现代JavaScript 应用程序的静态模块打包器

2.vue: 构建用户界面的渐进式框架(MVVM) 数据双向绑定

3.axios数据请求：基于Promise的HTTP请求库，在浏览器和node服务端上表现一致

4.element-ui：基于vue2.0的基于 Vue 2.0 的桌面端组件库

5.sass：强大的专业级CSS扩展语言

6.lodash: 一致性、模块化、高性能的 JavaScript 实用工具库。强大的数组、对象、字符串处理能力。

vuex: 是一个专为 Vue.js 应用程序开发的状态管理模式

7.vue-router: Vue 官方的路由管理器。它和 Vue.js 的核心深度集成，让构建单页面应用变得易如反掌。前端页面跳转的根本。

8.express：Express 是一个保持最小规模的灵活的 Node.js Web 应用程序开发框架，为 Web 和移动应用程序提供一组强大的功能。

9.express-http-proxy 通过中间件将请求代理到另一个主机并将响应传递回原始响应，解决了跨越问题


 **四、其他主要插件** 

1.Uweb： 友盟统计

2.dotenv：环境变量设置

3.eslint：前端代码规范插件

4.viewerjs： 强大的图片查看器，支持几乎所有的图片查看操作

5.moment：前端时间库，支持全部时间操作

6.xlsx：前端excel下载生成工具

 **五、package命令（项目从此开始）** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/141901_c2afda4d_1803822.png "屏幕截图.png")

1.

2.npm run dev 启动开发环境： 主要注意设置了环境变量development和设置其配置文件为webpack.dev.conf.js。 要求热启动。报错信息全。开发变化响应快。

3.npm run build:prod 生成生产环境中使用的源码：要求文件小，响应快，动态加载所属文件。关闭不重要报错。

4.npm run build: npm run build:prod 的别名

5.其他命令未使用或其他项目的命令1. 这里是列表文本

 **六、webpack项目配置（版本为：webpack: 4.16.5）** 

一)基本共享配置webpack.base.conf.js

1.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/142710_f0379618_1803822.png "屏幕截图.png")

2.content: 项目的目录

3.entry: 项目的入口文件：babel-prolyfill 浏览器js兼容修正。main.js 我们项目的入口，一切的开发都从这里开始。

4.output: 项目的出口，我们的文件输出到哪里。具体的在开发环境和生产环境讲解

5.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/142720_8851832e_1803822.png "屏幕截图.png")

6.webpack如何寻找模块对应的文件

7.extensions: webpack 找文件时自动匹配的文件后缀

8.alias: 配置项通过别名来把原来导入路径映射成一个新的导入路径，通俗来讲就是告诉webpack文件到哪里去找

9.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/142729_4bb2cacb_1803822.png "屏幕截图.png")

10.webpack 如何解析打包成浏览器/node 能识别的文件

11.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/142734_5605e749_1803822.png "屏幕截图.png")

12.plugins: 打包要用的插件。 vueLoaderPlugin 是解析vue 文件所必须的插件

13.externals：配置选项提供了「从输出的 bundle 中排除依赖」的方法。目的是引入外部库。在任何地方不需要引入直接使用。 这里配置了 jquery 和高德地图的插件。这样在vue文件中使用时就不会报错。

14.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/142742_b73cd16f_1803822.png "屏幕截图.png")禁用的node环境变量。原因图片中已有描述。目的是保持客户端的可用性： 因为客户端没有这些变量依赖

二)开发版的配置 webpack.dev.conf.js: 方便开发所需做的额外配置

1.这个文件继承了base的配置 

2.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143000_7aaa28c1_1803822.png "屏幕截图.png")


3.mode webpack 预设的环境，为方便开发设置了很多预制配置。开发版当然选择是delelopment(开发模式)

4.devtool: 开发的提示工具： 生成 sourceMap 方便开发查找问题是那个文件

5.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143032_f9aa085f_1803822.png "屏幕截图.png")

6.开发服务器配置：publicPath: 内存中的文件地址根路径，这里就是上面base中的output的publicPath。 proxy:我们要代理的地址，主要就是代理后端地址。 其他配置都很明显，见名知意。

7.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143045_c6b1ab91_1803822.png "屏幕截图.png")

8.DefinePlugin： 环境变量定义

9.HotModuleReplacementPlugin： 开发热更新

10.HtmlWebpackPlugin：html定义： 主要定义使用那个html， 编译后的名称。和如下的一点配置信息

![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143111_88e2441c_1803822.png "屏幕截图.png")

三)生产环境的 webpack.prod.conf.js 代码要求，浏览器能直接运行，简单，按需加载

1.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143209_4e6118e2_1803822.png "屏幕截图.png")

2.mode: 同开发设置，这是这里是生产环境

3.devtool: sourceMap: 一般关闭，线上有错误是开启，方便查找错误

4.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143214_44034e7f_1803822.png "屏幕截图.png")

5.文件输出到生产环境能用的地方，这里注意加上了[chunkhash:8] ，防止生产环境中的缓存

6.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143225_7d55149c_1803822.png "屏幕截图.png")

7.MiniCssExtractPlugin：css压缩插件，其他的和开发环境相同

8.NamedChunksPlugin：分片命名，保证分片稳定

9.CopyWebpackPlugin：静态文件拷贝插件

10.HashedModuleIdsPlugin： 文件内容稳定(hash值)，保证文件不随意变化

11.compression-webpack-plugin： 代码压缩插件

12.webpack-bundle-analyzer：可视的代码大小和结构插件

13.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143232_87dbc76b_1803822.png "屏幕截图.png")

14.wepack 自定义代码优化部分：目的打包时间更短，打出的包更小。一般按最优配置就行了。

四)对构建生产环境的扩展讲解： build/build.js

1.Dfs

2.我们的项目是支持热更新的。默认不支持，其实也没特别的技术。就是当编译成功后在去覆盖发布的源文件

3.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143330_08e6ba4b_1803822.png "屏幕截图.png")

1.rm 删除了当前编译环境的老代码

2.webpack 执行编译，生产编译文件，理论这里就结束了，下面是方便部署做的事

3.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143337_975b18b7_1803822.png "屏幕截图.png")

4.上面拷贝生产环境的部署代码，和源文件到指定的目录进行集中管理


 **七、服务器启动代码src/copy/index.js** 

1.本代码主要依赖了express 和express-http-proxy

2.express 渲染代码 和普通服务器一致

3.express-http-proxy 代理后端接口，目的是通过服务端解决前端的跨域名引起的各种问题，使用方式如下![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143418_1d813510_1803822.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143449_97985ebe_1803822.png "屏幕截图.png")

4. 这里表示不缓存index.html 保证更新后用户使用的是最新代码

5.依赖是同一个文件里的src/copy/package.json

 **八、服务器管理PM2
** 
1.配置文件 ecosystem.config.js

2.在测试环境和正式环境都是使用PM2 进行管理的： 自带进程守护，自动启动等

3.安装： npm  i  –g  pm2

4.常用命令 

a)pm2 list 查看当前所有应用

b)pm2 start [配置文件] 跟进配置文件启动应用

c)pm2 show [id] 查看应用的当前状态

d)pm2 restart [id] 重新启动当前应用： 本项目不需要，主要用于服务端渲染项目

e)pm2 stop [id] 停止当前应用

f)pm2 save 保存当前应用到文件中，自动重启会使用该文件

g)pm2 startup 生成守护进程： 只需要运行一次

5.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143535_d23d1ff5_1803822.png "屏幕截图.png")

6.apps 当前的应用数组，可配置多个

7.cmd：项目所在目录

8.args 运行项目的参数 上诉配置转化为命令是 cd cwd的目录然后执行 node index.js –p 3105 –u http//apiUrl 同服务器启动代码配合就可以启动项目了

9.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143540_4ce26b0b_1803822.png "屏幕截图.png")

10.这是是获取当前服务器的ip： 我们的项目是在不同的服务器链接不同的后端地址。这里动态获取可以保证开发一致性。当然也可以在 .env中配置

 **九、异常的整合** 

1.本项目对异常的整合非常的简单，就是是编译的代码支持子项目

2.在文件src/copy/index.js文件中
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143625_c8320343_1803822.png "屏幕截图.png")

3.第一行代码表示支持编译好的异常今天文件

4.第二行代码支持子项目的exception渲染解析

5.在本项目能够通过 项目域名/exception 渲染出异常项目就表示支持成功


十、项目的整体结构

1.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143734_936be65c_1803822.png "屏幕截图.png")

2.main.js 项目的整体入口文件

3.components 通用组件放置的位置

4.config 项目整体代码级配置：登陆 权限字段 后端代理等

5.copy 构建项目时拷贝到生产环境的代码

6.directive vue 指令放置的文件

7.filters 过滤器文件夹

8.router 路由文件夹

9.store 状态vuex文件夹

10.styles 样式源码文件夹

11.utils 所有的工具文件夹

a)ajax: 数据请求封装

b)auth: 用户信息获取

c)bus: 数据通信(,邪法,不推荐,很不好管理)  

d)Easemob: im聊天封装逻辑封装

e)emitter: vue 代码混合支持事件广播和事件派遣

f)globalComponents 全局组件注册

g)im: im聊天业务逻辑封装

h)index: 赋值方法

i)menuData menuDataFhb menuDataOms：菜单数据

j)openWindow: 项目自带打开新窗口的辅助函数

k)opermission: 权限封装

l)request: 项目自带的请求封装（未使用，我们使用的是ajax）

m)scroll-into-view: 浏览器滚动到自定原生工具,使用在组件下拉树（TreeSelect中）

n)selectOption: 下拉选项获取工具：一般用来获取后端字典，带参数的数组

o)StompJs: stompjs的封装，目的让代码只关注监听和销毁，不需要关注其他配置链接服务器

p)TimeReduce: 倒计时的简单封装：未使用

q)validate validateUtil: 自定义的验证器，验证方法

12.vendor: 扩展文件夹 系统自带: 用于导出excel和压缩文件

13.views： 业务页面放置的文件夹，通过和路由配置实现页面的跳转

14.App.vue: 组件的通用入口

15.permission: 权限控制的工具

 **

### 十一、项目代码入口讲解main.js




** 

1.main.js 是整个项目开始的地方。是在webpack.base.conf.js 中配置的

2.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143849_56716fc2_1803822.png "屏幕截图.png")

3.上图表示了引入各种依赖

a)normalize.css: 重置不同浏览器的样式，保证样式一致（99%）

b)Element: 框架使用的主要组件库

c)vue-amap: 高德地图组件库

d)*scss .css: 系统的样式文件

e)App 项目组件入口（vue）

f)router:路由依赖

i.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/143855_cad8099b_1803822.png "屏幕截图.png")

ii.Vue、Router： 官方路由依赖

iii.constantRouterMap：静态的，不需要权限就能访问的路由（和我们的权限系统配合）

iv.asyncRouterMap：动态的路由，需要登陆后有菜单权限茶能访问的路由

v.Layout：布局文件： 主要要来继承菜单

g)store: 状态管理器

h)i18n: 语言本地化工具：（我们未使用）

i)icons: 官方默认字体

j)errorLog: 错误信息收集（我们未使用）

k)permission：权限管理控制

l)filters：过滤器控制

m)ajax：我们的数据请求封装

n)globalComponents：注册全局组件

o)lodash：js插件库

p)vue-uweb：友盟统计

### 十二、主要组件封装讲解


一)Ajax 封装 src/utils/ajax

1.简述：ajax的底层基础是axios

2.封装的要点

a)支持请求拦截： 设置后端请求要求的各种参数：token, menu_id,过滤请求中多余的参数

b)支持响应拦截：拦截后端请求的异常，统一处理(400, 401, 503...),批量过滤后端返回的null

c)自动完成全局的配置： 添加/api 全局拦截退出

d)方便的请求 get，post, 可扩展的配置

3.![](https://images.gitee.com/uploads/images/2019/0212/144258_2eae53df_1803822.png "屏幕截图.png")			
完成了token设置，菜单id的设置和过滤掉空的请求参数
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/144349_a03dfd3f_1803822.png "屏幕截图.png")

4.完成了null值替换和统一的异常处理，统一提示出后端的报错信息


5.![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/144427_df3c1cd0_1803822.png "屏幕截图.png")配置默认值，已经非常的方便能进行get请求了

6.post 请求也就是多了个请求的表单数据。

7.由于老框架的使用习惯这里也进行了用户信息的/登陆/获取的方法映射
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/144448_ff1d1c34_1803822.png "屏幕截图.png")

8.这里比较复杂的就是表格数据请求了(其实也是非常的简单) 
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/161455_af03df96_1803822.png "屏幕截图.png")

主要就是在get 方法上进行了分页参数的设置，分页数据的设置

9.最后为了方便在每个页面(vue)使用我们进行了vue 安装配置
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/161516_1a5555de_1803822.png "屏幕截图.png")

二)Auth 用户信息统一管理

1.简述：我们的用户中心是一个相对复杂的流程，授权呀，登陆呀，拦截呀，修改，权限等...。所以我们提炼出单独的文件进行统一管理，方便以后进行统一调整

2.用户的统一配置，批量修改，方便引用![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/161636_23c71799_1803822.png "屏幕截图.png")

3.标准的登陆，登出，获取用户信息的方法![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/161641_b6fa7e5b_1803822.png "屏幕截图.png")

4.前端的权限写入，响应的修改用户信息，获取用户信息![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/161648_853639b0_1803822.png "屏幕截图.png")

5.项目的特殊处，后端要求在未登陆时传入用户信息的相关配置
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/161657_4b049c78_1803822.png "屏幕截图.png")

五)opermission 权限的封装src/permisssion

1.简述：所谓权限，就是根据后端的数据，进行访问的允许与禁止
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/161917_f49b7da5_1803822.png "屏幕截图.png")

a)判断用户是否登陆了，没有登陆就跳转登陆界面

i.getToken判断用户是否登陆

ii.允许白名单机制，不登陆也可以访问一些界面

iii.跳转登陆进行登陆

b)登陆了去后端获取权限数据
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/161926_60358176_1803822.png "屏幕截图.png")

c)这里进行异步数据获取，因为我们不知道何时能获取成功，所以在不停的等待。等待一秒中，设置了最大的重试次数

d)![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/161934_09073665_1803822.png "屏幕截图.png")

e)注意这里，我们获取数据成功后把数据交给了vuex。并且动态的根据菜单数据添加可以访问的的路由

六)StompJs封装讲解utils/StompJs.js

1.Stompjs 是同服务端及时通讯的工具：后端告诉前端发生了莫个事件，然后前端进行响应

2.封装的目的就是在要关注的地方直接关注就好了

3.单例模式，在任何地方使用都是一个实例
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/161942_67a2385a_1803822.png "屏幕截图.png")

4.获取同服务器的链接批量关注消息时使用
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/161948_e6543f18_1803822.png "屏幕截图.png")

5.如果只快速的关注一个消息直接使用，可以自动获取同服务器的链接
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/161955_1f0df556_1803822.png "屏幕截图.png")

6.当不需要关注的时候，我们可以销毁关注，节约内存开销
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162001_904b2045_1803822.png "屏幕截图.png")

7.使用事例
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162008_f0710828_1803822.png "屏幕截图.png")

a)单个使用

b)批量使用
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162016_0d0e4c2f_1803822.png "屏幕截图.png")

七)BasicTable组件封装

1.简述：为了实现快速的表格复用和实现大量简单的工作进行了此封装

a)自动的分页功能： 提前准备分页组件

b)可配置的后端地址接口：数据要求规范，和后端达成一致

c)可以插入操作功能

d)Excel导出功能

e)字段隐藏和展示功能

f)拷贝功能

2.分页的封装： 分页需要的是总条数,一页展示的数量和当前页数
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162029_8482259a_1803822.png "屏幕截图.png")

3.获取后端数据
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162035_58a3be71_1803822.png "屏幕截图.png")

4.分页和切换页数和搜索     
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162040_a430ea38_1803822.png "屏幕截图.png")
 
5.下载excel

a)准备下载的数据， 这里不用后端的数据的原因是，前端会对数据进行格式化，所以使用了已经渲染好了的数据

![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162046_cad8c3e6_1803822.png "屏幕截图.png")

b)引入下载依赖： 这里使用了动态加载，在用户真正要下载的时候我们在引入
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162050_fe4a280d_1803822.png "屏幕截图.png")

c)这里面进行的封装下载vendor/Export2Excel
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162106_d562c055_1803822.png "屏幕截图.png")

6.隐藏与展示选择了列

a)表格所需做的事情就是根据字段判断是否需要隐藏
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162113_31f495c1_1803822.png "屏幕截图.png")

b)这里统计计算的方式计算处隐藏的功能
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162119_ffb9eee7_1803822.png "屏幕截图.png")

c)封装了一个弹窗组件用于设置隐藏和展示src/components/BasicTable/VisibleModal
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162132_210adb0f_1803822.png "屏幕截图.png")

d)把设置的数据保存在本地，进行持久化
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162139_4af2fed2_1803822.png "屏幕截图.png")

7.拷贝功能
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162145_681b62bc_1803822.png "屏幕截图.png")

a)处理要拷贝的数据


十三、Git的项目管理

1.Git 是一个功能很完善的版本管理工具。具体特点就不用说了。

2.我们的具体要做的事一般就是创建项目

a)前置条件： 添加了用户组，要使用用户组

b)创建项目
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162238_85c42689_1803822.png "屏幕截图.png")

c)权限选择
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162242_4f797d54_1803822.png "屏幕截图.png")

d)创建项目后初始化项目
![输入图片说明](https://images.gitee.com/uploads/images/2019/0212/162257_585a94a2_1803822.png "屏幕截图.png")
e)一般选择第二中，我们一般会用vue工具初始化一个空项目

f)然后就可以上传下载了。

3.常用命令

a)git add . 添加全部变化的文件到待提交版本

b)git commit –m ‘描述’ 提交到当前版本

c)git push 把本地的变化推送到服务器

d)git status 查看当前版本的状态，添加了什么文件，删除了什么，修改了什么

e)scp(不是git命令，用来修正线上文件)

4.我们没有做好的

a)线上版本发布，经常要单个文件修正

i.解决： 新增一个待发布的分支，而不是我们的开发分支继续发布（一直比较忙，没有做，有点拖延）

5.我的默认操作

a)设置项目为组项目

b)每个项目都是用户组的私有项目：防止无权限人员误操作

c)为开发者添加开发者权限

