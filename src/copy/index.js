var express = require('express');
var proxy = require('express-http-proxy');
let path = require('path');
var program = require('commander');
var colors = require('colors');

function url(val) {
  return val
}

function make_red(txt) {
  return colors.red(txt); //display the help text in red on the console
}

program
  .version('0.0.1')
  .option('-p, --port <n>', 'service listen port', parseInt)
  .option('-u, --url [value]', 'java api site')
  .parse(process.argv);

const port = program.port
const apiUrl = program.url

if (!port) {
  console.log(make_red('port can\'t empty'))
  process.exit(-1)
}

if (!apiUrl) {
  console.log(make_red('java api url can\'t empty'))
  process.exit(-1)
}

var app = express({timeout: 600000});

// 后端接口代理
app.use('/api', proxy(apiUrl, {
  limit: '10mb',
  proxyReqPathResolver: function (req, res) {
    return require('url').parse(req.url).path;
  }
}));

// 静态资源
app.use('/static', express.static('static'), express.static('exception/static'));

app.get('/exception', function (req, res) {
  res.sendFile(path.resolve('exception/index.html'), {etag: false, lastModified: false});
})

app.get('*', function (req, res) {
  res.sendFile(path.resolve('index.html'), {etag: false, lastModified: false});
})

const server = app.listen(port, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});


server.setTimeout(600000, function () {
  console.log('超时啦 setTimeout')
})
