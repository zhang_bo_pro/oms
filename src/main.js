import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import Element from 'element-ui'
import '@/styles/theme-chalk/src/index.scss'
import 'element-ui/lib/theme-chalk/display.css'
import 'viewerjs/dist/viewer.css'

import '@/styles/index.scss' // global css

import App from './App'
import router from './router'
import store from './store'

import i18n from './lang' // Internationalization
import './icons' // icon
import './errorLog' // error log
import './permission' // permission control
// import './mock' // simulation data

import * as filters from './filters' // global filters
import ajax from '@/utils/ajax'
import globalComponents from '@/utils/globalComponents'
import _ from 'lodash'
import uweb from 'vue-uweb'

Object.defineProperty(Vue.prototype, '$_', { value: _ })
Vue.use(ajax)
Vue.use(Element, {size: 'medium'}) // , {size: 'medium'}
/**
 * Web端产品添加友盟统计代码：
 * 居家小二SaaS云：友盟 id=1275767439
 * 发货宝：友盟 id=1275767468
 */
const projectType = process.env.projectType
Vue.use(uweb, projectType === 'fhbMant' ? '1275767468' : '1275767439')

// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

// 全局注册组件
Object.keys(globalComponents).forEach(key => {
  Vue.component(key, globalComponents[key])
})

Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App)
})
