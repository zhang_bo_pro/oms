import Vue from 'vue'
import Router from 'vue-router'
/* Layout */
import Layout from '@/views/layout/Layout'

Vue.use(Router)
/* 发货宝路由 */
export const constantRouterMap = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/authredirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/errorPage/404'),
    hidden: true,
    meta: {title: '404', noCache: true}
  },
  {
    path: '/401',
    component: () => import('@/views/errorPage/401'),
    hidden: true,
    meta: {title: '401', noCache: true}
  },
  {
    path: '/test',
    component: () => import('@/views/test/Index'),
    hidden: true,
    meta: {title: '测试', noCache: true}
  },
  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/fhb/Index'),
        name: 'fhbDashboard',
        meta: {title: '首页', icon: 'dashboard', noCache: true}
      }
    ]
  }
]

export const asyncRouterMap = [
  {
    path: '/layout',
    component: Layout,
    children: [
      {
        name: 'orderCreateService',
        path: '/ss-fhb/test',
        component: () => import('@/views/fhb/Test'),
        meta: {title: '测试', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'myOrdersOfferingIndex',
        path: '/ss-fhb/myOrders/offering/index',
        component: () => import('@/views/fhb/myOrders/offering/Index'),
        meta: {title: '抢单中', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'myOrdersOfferingShow',
        path: '/ss-fhb/myOrders/offering/show/:id',
        component: () => import('@/views/fhb/myOrders/offering/Show'),
        meta: {title: '抢单中-订单详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'myOrdersPendingdIndex',
        path: '/ss-fhb/myOrders/pending/index',
        component: () => import('@/views/fhb/myOrders/pending/Index'),
        meta: {title: '待付款', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'myOrdersPendingShow',
        path: '/ss-fhb/myOrders/pending/show/:id',
        component: () => import('@/views/fhb/myOrders/pending/Show'),
        meta: {title: '待付款-订单详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'myOrdersInServiceIndex',
        path: '/ss-fhb/myOrders/inService/index',
        component: () => import('@/views/fhb/myOrders/inService/Index'),
        meta: {title: '服务中', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'myOrdersInServiceShow',
        path: '/ss-fhb/myOrders/inService/show/:id',
        component: () => import('@/views/fhb/myOrders/inService/Show'),
        meta: {title: '服务中-订单详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'myOrdersConfirmingIndex',
        path: '/ss-fhb/myOrders/confirming/index',
        component: () => import('@/views/fhb/myOrders/confirming/Index'),
        meta: {title: '待确认', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'myOrdersConfirmingShow',
        path: '/ss-fhb/myOrders/confirming/show/:id',
        component: () => import('@/views/fhb/myOrders/confirming/Show'),
        meta: {title: '待确认-订单详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'myOrdersCompletedIndex',
        path: '/ss-fhb/myOrders/completed/index',
        component: () => import('@/views/fhb/myOrders/completed/Index'),
        meta: {title: '已完成', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'myOrdersCompletedShow',
        path: '/ss-fhb/myOrders/completed/show/:id',
        component: () => import('@/views/fhb/myOrders/completed/Show'),
        meta: {title: '已完成-订单详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'myOrdersOfferingPayment',
        path: '/ss-fhb/myOrders/offering/payment/:id',
        component: () => import('@/views/fhb/myOrders/Payment'),
        meta: {title: '支付', icon: 'list'},
        hidden: true
      },
      {
        name: 'myOrdersPendingPayment',
        path: '/ss-fhb/myOrders/pending/payment/:id',
        component: () => import('@/views/fhb/myOrders/Payment'),
        meta: {title: '支付', icon: 'list'},
        hidden: true
      },
      {
        name: 'myOrdersInservicePaymentAdditional',
        path: '/ss-fhb/myOrders/inService/paymentAdditional/:id',
        component: () => import('@/views/fhb/myOrders/PaymentAdditional'),
        meta: {title: '追加费用', icon: 'list'},
        hidden: true
      },
      {
        name: 'myOrdersConfirmingPaymentAdditional',
        path: '/ss-fhb/myOrders/confirming/paymentAdditional/:id',
        component: () => import('@/views/fhb/myOrders/PaymentAdditional'),
        meta: {title: '追加费用', icon: 'list'},
        hidden: true
      },
      {
        name: 'myOrdersDetailMergePay',
        path: '/ss-fhb/myOrders/mergePay/:ids',
        component: () => import('@/views/fhb/myOrders/mergePay'),
        meta: {title: '合并支付', icon: 'list'},
        hidden: true
      },
      // 订单查询
      {
        name: 'orderIndex',
        path: '/ss-fhb/order/index',
        component: () => import('@/views/fhb/order/Index'),
        meta: {title: '订单查询', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'orderShow',
        path: '/ss-fhb/order/show/:id',
        component: () => import('@/views/fhb/order//Show'),
        meta: {title: '订单详情', icon: 'list'},
        hidden: true
      },
      // 待发起
      {
        name: 'paymentApprovePendingIndex',
        path: '/ss-fhb/paymentApprove/pending/index',
        component: () => import('@/views/fhb/paymentApprove/pending/Index'),
        meta: {title: '待支付', icon: 'list'},
        alwaysShow: true
      },
      // 待发起详情
      {
        name: 'paymentApprovePendingShow',
        path: '/ss-fhb/paymentApprove/pending/show/:id',
        component: () => import('@/views/fhb/paymentApprove/pending/Show'),
        meta: {title: '待支付详情', icon: 'list'},
        hidden: true
      },
      // 待审批
      {
        name: 'paymentApproveApproveIndex',
        path: '/ss-fhb/paymentApprove/approve/index',
        component: () => import('@/views/fhb/paymentApprove/approve/Index'),
        meta: {title: '待审批', icon: 'list'},
        alwaysShow: true
      },
      // 待审批详情
      {
        name: 'paymentApproveApproveShow',
        path: '/ss-fhb/paymentApprove/approve/show/:id',
        component: () => import('@/views/fhb/paymentApprove/approve/Show'),
        meta: {title: '待审批详情', icon: 'list'},
        hidden: true
      },
      // 待支付
      {
        name: 'paymentApprovePaymentIndex',
        path: '/ss-fhb/paymentApprove/payment/index',
        component: () => import('@/views/fhb/paymentApprove/payment/Index'),
        meta: {title: '待支付', icon: 'list'},
        alwaysShow: true
      },
      // 待支付详情
      {
        name: 'paymentApprovePaymentShow',
        path: '/ss-fhb/paymentApprove/payment/show/:id',
        component: () => import('@/views/fhb/paymentApprove/payment/Show'),
        meta: {title: '待支付详情', icon: 'list'},
        hidden: true
      },
      // 已支付详情
      {
        name: 'paymentApprovePaymentAlreadyPay',
        path: '/ss-fhb/paymentApprove/payment/alreadyPay/:id',
        component: () => import('@/views/fhb/paymentApprove/payment/AlreadyPay'),
        meta: {title: '已支付详情', icon: 'list'},
        hidden: true
      },
      // 支付
      {
        name: 'paymentApprovePaymentPayment',
        path: '/ss-fhb/paymentApprove/payment/payment/:id',
        component: () => import('@/views/fhb/paymentApprove/payment/Payment'),
        meta: {title: '支付', icon: 'list'},
        hidden: true
      },
      // 我的账号
      {
        name: 'myAccountIndex',
        path: '/ss-fhb/myAccount/index',
        component: () => import('@/views/fhb/myAccount/Index'),
        meta: {title: '我的账号', icon: 'list'},
        alwaysShow: true
      },
      // 我的账号-改密码
      {
        name: 'myAccountChangePsw',
        path: '/ss-fhb/myAccount/changePsw/:id',
        component: () => import('@/views/fhb/myAccount/ChangePsw'),
        meta: {title: '我的账号-改密码', icon: 'list', noCache: true},
        hidden: true
      },
      // 我的账号-改绑手机号
      {
        name: 'myAccountChangePhone',
        path: '/ss-fhb/myAccount/change-phone/:phone',
        component: () => import('@/views/fhb/myAccount/ChangePhone'),
        meta: {title: '我的账号-改绑手机', icon: 'list', noCache: true},
        hidden: true
      },
      {
        name: 'faTemporaryIndex',
        path: '/ss-fhb/fa-temporary/index',
        component: () => import('@/views/fhb/temporary/Index'),
        meta: {title: '订单池', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'faTemporaryShow',
        path: '/ss-fhb/fa-temporary/show/:id',
        component: () => import('@/views/fhb/temporary/Show'),
        meta: {title: '订单池-详情', icon: 'list', noCache: true},
        hidden: true
      },
      {
        name: 'faTemporaryInput',
        path: '/ss-fhb/fa-temporary/show',
        component: () => import('@/views/fhb/temporary/Input'),
        meta: {title: '录单', icon: 'list', noCache: true},
        hidden: true
      },
      // 企业认证 -列表
      {
        name: 'companyCertificationIndex',
        path: '/ss-fhb/companyCertification/index',
        component: () => import('@/views/fhb/companyCertification/Index'),
        meta: {title: '企业认证', icon: 'list'},
        alwaysShow: true
      },
      // 企业认证 -新增
      {
        name: 'companyCertificationEdit',
        path: '/ss-fhb/companyCertification/edit/:businessId',
        component: () => import('@/views/fhb/companyCertification/Edit'),
        meta: {title: '企业认证-编辑', icon: 'list', noCache: true}
      },
      // 企业认证 -认证详情
      {
        name: 'companyCertificationShow',
        path: '/ss-fhb/companyCertification/show/:id',
        component: () => import('@/views/fhb/companyCertification/Show'),
        meta: {title: '企业认证-详情', icon: 'list', noCache: true}
      },
      // 企业认证 -认证详情
      {
        name: 'reportIndex',
        path: '/ss-fhb/report/index/:id',
        component: () => import('@/views/report/Index'),
        meta: {title: '报表', icon: 'list'}
      },
      // 提货地址管理 -列表
      {
        name: 'pickAddressIndex',
        path: '/ss-fhb/pickAddress/index',
        component: () => import('@/views/fhb/pickAddress/Index'),
        meta: {title: '提货地址管理', icon: 'list'},
        alwaysShow: true
      },
      // 售后服务 退款
      {
        name: 'refundIndex',
        path: '/ss-fhb/refund/index',
        component: () => import('@/views/fhb/refund/Index'),
        meta: {title: '退款', icon: 'list'}
      },
      // 售后服务 退款
      {
        name: 'refundShow',
        path: '/ss-fhb/refund/show/:id',
        component: () => import('@/views/fhb/refund/Show'),
        meta: {title: '退款详情', icon: 'list', noCache: true}
      },
      // 售后服务 投诉管理
      {
        name: 'fhbComplaintIndex',
        path: '/ss-fhb/complaint',
        component: () => import('@/views/fhb/complaint/Index'),
        meta: {title: '投诉管理', icon: 'list'}
      },
      // 售后服务 投诉管理
      {
        name: 'fhbComplaintShow',
        path: '/ss-fhb/complaint/show/:id',
        component: () => import('@/views/fhb/complaint/Show'),
        meta: {title: '投诉管理详情', icon: 'list', noCache: true}
      },
      { // 账号管理
        path: '/ss-fhb/account/index',
        name: 'fhbAccountIndex',
        component: () => import('@/views/fhb/account/Index'),
        meta: {title: '账号管理', icon: 'list'}
      },
      { // 新增-账号管理
        path: '/ss-fhb/account/create',
        name: 'fhbAccountCreate',
        component: () => import('@/views/fhb/account/Create'),
        meta: {title: '账号新增', icon: 'list', noCache: true}
      },
      { // 修改-账号管理
        path: '/ss-fhb/account/edit/:id',
        name: 'fhbAccountEdit',
        component: () => import('@/views/fhb/account/Create'),
        meta: {title: '账号编辑', icon: 'list', noCache: true}
      },
      { // 详情-账号管理详情
        path: '/ss-fhb/account/show/:id',
        name: 'fhbAccountShow',
        component: () => import('@/views/fhb/account/Show'),
        meta: {title: '账号详情', icon: 'list', noCache: true}
      },
      { // 我的师傅
        path: '/ss-fhb/myMaster/index',
        name: 'fhbMyMasterIndex',
        component: () => import('@/views/fhb/myMaster/Index'),
        meta: {title: '我的师傅', icon: 'list'}
      },
      {
        name: 'myWalletIndex',
        path: '/ss-fhb/myWallet/index',
        component: () => import('@/views/fhb/myWallet/Index'),
        meta: {title: '我的钱包', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'myWalletSetPsdShow',
        path: '/ss-fhb/myWallet/setPsdShow',
        component: () => import('@/views/fhb/myWallet/SetPsdShow'),
        meta: {title: '钱包密码', icon: 'list', noCache: true},
        hidden: true
      },
      {
        name: 'logListIndex',
        path: '/ss-fhb/my-wallet/log-list',
        component: () => import('@/views/fhb/myWallet/LogList'),
        meta: {title: '账单流水列表', icon: 'list'},
        hidden: true
      },
      {
        name: 'bankCardIndex',
        path: '/ss-fhb/myWallet/bankCardIndex',
        component: () => import('@/views/fhb/myWallet/BankCardIndex'),
        meta: {title: '银行卡列表', icon: 'list', noCache: true},
        hidden: true
      },
      {
        name: 'addBankCard',
        path: '/ss-fhb/myWallet/addBankCard',
        component: () => import('@/views/fhb/myWallet/AddBankCard'),
        meta: {title: '添加银行卡', icon: 'list', noCache: true},
        hidden: true
      },
      {
        name: 'couponList',
        path: '/ss-fhb/my-wallet/couponList',
        component: () => import('@/views/fhb/myWallet/CouponList'),
        meta: {title: '优惠券列表', icon: 'list'},
        hidden: true
      },
      {
        name: 'couponShow',
        path: '/ss-fhb/my-wallet/coupon-show/:id',
        component: () => import('@/views/fhb/myWallet/CouponShow'),
        meta: {title: '优惠券详情', icon: 'list', noCache: true},
        hidden: true
      },
      {
        name: 'myWalletRecharge',
        path: '/ss-fhb/myWallet/recharge/:inputMoney',
        component: () => import('@/views/fhb/myWallet/Recharge'),
        meta: {title: '充值', icon: 'list'},
        hidden: true
      },
      { // 我的产品
        path: '/ss-fhb/product/manage',
        name: 'productManage',
        component: () => import('@/views/fhb/product/Manage'),
        meta: {title: '我的产品', icon: 'list'}
      },
      {
        name: 'systemNotice',
        path: '/ss-fhb/systemNotice',
        component: () => import('@/views/fhb/systemNotice/Index'),
        meta: {title: '消息中心', icon: 'list'},
        alwaysShow: true
      }
    ]
  },
  {
    path: '*',
    component: () => import('@/views/errorPage/404'),
    hidden: true
  }
]

export default new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({y: 0}),
  routes: constantRouterMap
})
