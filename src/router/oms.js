import Vue from 'vue'
import Router from 'vue-router'
/* Layout */
import Layout from '@/views/layout/Layout'

Vue.use(Router)
/* oms 路由 */
export const constantRouterMap = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/register',
    component: () => import('@/views/login/OmsRegister')
  },
  {
    path: '/edit-company',
    component: () => import('@/views/login/EditCompany')
  },
  {
    path: '/examining',
    component: () => import('@/views/login/Examining')
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/authredirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/errorPage/404'),
    hidden: true,
    meta: {title: '404', noCache: true}
  },
  {
    path: '/401',
    component: () => import('@/views/errorPage/401'),
    hidden: true,
    meta: {title: '401', noCache: true}
  },
  {
    path: '/test',
    component: Layout,
    children: [
      {
        name: 'testIndex',
        path: '/test',
        component: () => import('@/views/test/Index'),
        meta: {title: '测试', noCache: true}
      }
    ]
  },
  {
    path: '/ss-oms/any-report',
    component: Layout,
    children: [
      {
        name: 'anyReportIndex',
        path: '/ss-oms/any-report/:id',
        component: () => import('@/views/anyReport/Index'),
        meta: {title: '报表系统', noCache: true, iFrame: true}
      }
    ]
  },
  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: {title: '首页', icon: 'dashboard', noCache: true}
      }
    ]
  }
]

export const asyncRouterMap = [
  {
    path: '/layout',
    component: Layout,
    children: [
      {
        name: 'orderCreateService',
        path: '/ss-oms/order/create-service',
        component: () => import('@/views/oms/order/CreateService'),
        meta: {title: '订单录入', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'orderTempService',
        alwaysShow: true,
        path: '/ss-oms/order/temp-service',
        component: () => import('@/views/oms/order/TempService'),
        meta: {title: '订单录入暂存', icon: 'list'}
      },
      {
        name: 'orderTempServiceShow',
        hidden: true,
        path: '/ss-oms/order/temp-service-show/:id',
        component: () => import('@/views/oms/order/CreateService'),
        meta: {title: '暂存详情', icon: 'list', noCache: true}
      },
      {
        name: 'serviceOrdersEditIndex',
        alwaysShow: true,
        path: '/ss-oms/orders/service-orders/edit-index',
        component: () => import('@/views/oms/orders/serviceOrders/EditIndex'),
        meta: {title: '服务订单修改', icon: 'list'}
      },
      {
        name: 'serviceOrdersEditShow',
        hidden: true,
        path: '/ss-oms/orders/service-orders/edit-show/:id',
        component: () => import('@/views/oms/order/CreateService'),
        meta: {title: '服务订单修改详情', icon: 'list'}
      },
      { // 服务订单编辑
        path: '/ss-oms/orders/service-orders/edit/:id',
        name: 'orderServiceOrdersEdit',
        hidden: true,
        component: () => import('@/views/oms/order/CreateService'),
        meta: {title: '服务订单编辑', icon: 'list'}
      },
      {
        name: 'userPickIndex',
        path: '/ss-oms/orders/user-pick/index',
        component: () => import('@/views/oms/orders/userPick/Index'),
        meta: {title: '用户自提单', icon: 'list'},
        alwaysShow: true
      },
      // 入库订单-列表
      {
        path: '/ss-oms/enterWarehouse/warehousing-order/edit-index',
        name: 'warehousingOrderEditIndex',
        component: () => import('@/views/oms/enterWarehouse/warehousingOrder/EditIndex'),
        meta: {title: '入库订单', icon: 'list'}
      },
      {
        path: '/ss-oms/enterWarehouse/warehousing-order/edit-show/:id',
        name: 'warehousingOrderEditShow',
        component: () => import('@/views/oms/enterWarehouse/warehousingOrder/Show'),
        hidden: true,
        meta: {title: '入库订单详情', icon: 'list', noCache: true}
      },
      {
        path: '/ss-oms/enterWarehouse/warehousing-order/edit/:id',
        name: 'warehousingOrderEdit',
        component: () => import('@/views/oms/order/Create'),
        hidden: true,
        meta: {title: '入库订单修改', icon: 'list', noCache: true}
      },
      {
        name: 'orderCreate',
        alwaysShow: true,
        path: '/ss-oms/order/create',
        component: () => import('@/views/oms/order/Create'),
        meta: {title: '新品入库', icon: 'list'}
      },
      {
        name: 'orderCreateOut',
        alwaysShow: true,
        path: '/ss-oms/order/create-out',
        component: () => import('@/views/oms/order/CreateOut'),
        meta: {title: '出库订单', icon: 'list', noCache: true}
      },
      {
        path: '/ss-oms/order/temp',
        name: 'orderTemp',
        component: () => import('@/views/oms/order/Temp'),
        meta: {title: '新品入库暂存', icon: 'list'}
      },
      {
        path: '/ss-oms/order/temp-out',
        name: 'orderTempOut',
        component: () => import('@/views/oms/order/TempOut'),
        meta: {title: '出库暂存', icon: 'list'}
      },
      // 服务订单
      {
        path: '/ss-oms/orders/service-orders/index',
        name: 'serviceOrdersIndex',
        component: () => import('@/views/oms/orders/serviceOrders/Index'),
        meta: {title: '订单查询', icon: 'list'}
      },
      {
        path: '/ss-oms/orders/service-orders/show/:id',
        name: 'serviceOrdersShow',
        hidden: true,
        component: () => import('@/views/oms/orders/serviceOrders/Show'),
        meta: {title: '订单详情', icon: 'list'}
      },
      { // 服务订单导出
        path: '/ss-oms/orders/service-orders/export',
        name: 'serviceOrdersExport',
        component: () => import('@/views/oms/orders/serviceOrders/Export'),
        meta: {title: '服务订单导出', icon: 'list'}
      },
      {
        path: '/ss-oms/order/temp-show/:id',
        name: 'orderTempShow',
        hidden: true,
        component: () => import('@/views/oms/order/Create'),
        meta: {title: '入库暂存修改', icon: 'list'}
      },
      {
        path: '/ss-oms/order/temp-out-show/:id',
        name: 'orderTempOutShow',
        hidden: true,
        component: () => import('@/views/oms/order/CreateOut'),
        meta: {title: '出库暂存详情', icon: 'list'}
      },
      {
        name: 'arriveManager',
        path: '/ss-oms/orders/arrive-manager/index',
        component: () => import('@/views/oms/orders/arriveOrders/Index'),
        meta: {title: '到件管理', icon: 'list'},
        alwaysShow: true
      },
      {
        path: '/ss-oms/orders/arrive-manager/show/:id',
        name: 'arriveOrdersShow',
        hidden: true,
        component: () => import('@/views/oms/orders/arriveOrders/Show'),
        meta: {title: '到件管理详情', icon: 'list'}
      },
      {
        path: '/ss-oms/orders/arrive-manager/arrive/:id',
        name: 'arriveOrdersArrive',
        hidden: true,
        component: () => import('@/views/oms/orders/arriveOrders/Arrive'),
        meta: {title: '到件管理到件', icon: 'list'}
      },
      {
        path: '/ss-oms/orders/time-further/index',
        name: 'timeFurtherIndex',
        component: () => import('@/views/oms/orders/timeFurther/Index'),
        meta: {title: '时效管理', icon: 'list'},
        alwaysShow: true
      },
      {
        path: '/ss-oms/orders/time-further/show/:id',
        name: 'timeFurtherShow',
        hidden: true,
        component: () => import('@/views/oms/orders/serviceOrders/Show'),
        meta: {title: '订单详情', icon: 'list', noCache: true}
      },
      {
        name: 'warehouseAllocationIndex',
        path: '/ss-oms/enterWarehouse/warehouse-allocation/index',
        component: () => import('@/views/oms/enterWarehouse/warehouseAllocation/Index'),
        meta: {title: '入库库位分配列表', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'warehouseAllocationShow',
        path: '/ss-oms/enterWarehouse/warehouse-allocation/show/:id',
        component: () => import('@/views/oms/enterWarehouse/warehouseAllocation/Show'),
        meta: {title: '入库库位分配', icon: 'list'},
        hidden: true
      },
      {
        name: 'warehouseAllocationCdIndex',
        path: '/ss-oms/enterWarehouse/warehouse-allocation-cd/index',
        component: () => import('@/views/oms/enterWarehouse/warehouseAllocationCd/Index'),
        meta: {title: '入库库位分配CD列表', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'warehouseAllocationCdShow',
        path: '/ss-oms/enterWarehouse/warehouse-allocation-cd/show/:id',
        component: () => import('@/views/oms/enterWarehouse/warehouseAllocationCd/Show'),
        meta: {title: '入库库位分配CD', icon: 'list'},
        hidden: true
      },
      {
        name: 'warehousingOrderIndex',
        path: '/ss-oms/enterWarehouse/warehousing-order/index',
        component: () => import('@/views/oms/enterWarehouse/warehousingOrder/Index'),
        meta: {title: '入库单列表', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'warehousingOrderShow',
        hidden: true,
        path: '/ss-oms/enterWarehouse/warehousing-order/show/:id',
        component: () => import('@/views/oms/enterWarehouse/warehousingOrder/Show'),
        meta: {title: '入库单详情', icon: 'list', noCache: true}
      },
      {
        name: 'enterWarehouseConfirmIndex',
        path: '/ss-oms/enterWarehouse/warehouse-confirm/index',
        component: () => import('@/views/oms/enterWarehouse/warehouseConfirm/Index'),
        meta: {title: '入库确认', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'enterWarehouseConfirmShow',
        hidden: true,
        path: '/ss-oms/enterWarehouse/warehouse-confirm/show/:id',
        component: () => import('@/views/oms/enterWarehouse/warehouseConfirm/Show'),
        meta: {title: '入库确认详情', icon: 'list', noCache: true}
      },
      {
        name: 'enterWarehousePicklistIndex',
        path: '/ss-oms/enterWarehouse/warehouse-picklist/index',
        component: () => import('@/views/oms/enterWarehouse/warehousePicklist/Index'),
        meta: {title: '入库拣货单', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'enterWarehousePicklistShow',
        hidden: true,
        path: '/ss-oms/enterWarehouse/warehouse-picklist/show/:id',
        component: () => import('@/views/oms/enterWarehouse/warehousePicklist/Show'),
        meta: {title: '入库拣货单-详情', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'outputWarehouseAllocationIndex',
        path: '/ss-oms/outputWarehouse/warehouse-allocation/index',
        component: () => import('@/views/oms/outputWarehouse/warehouseAllocation/Index'),
        meta: {title: '出库库位分配列表', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'outputWarehouseAllocationShow',
        path: '/ss-oms/outputWarehouse/warehouse-allocation/show/:id',
        component: () => import('@/views/oms/outputWarehouse/warehouseAllocation/Show'),
        meta: {title: '出库库位分配', icon: 'list'},
        hidden: true
      },
      {
        name: 'outputWarehouseAllocationCdIndex',
        path: '/ss-oms/outputWarehouse/warehouse-allocation-cd/index',
        component: () => import('@/views/oms/outputWarehouse/warehouseAllocationCd/Index'),
        meta: {title: '出库库位分配CD列表', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'outputWarehouseAllocationCdShow',
        path: '/ss-oms/outputWarehouse/warehouse-allocation-cd/show/:id',
        component: () => import('@/views/oms/outputWarehouse/warehouseAllocationCd/Show'),
        meta: {title: '出库库位分配CD', icon: 'list'},
        hidden: true
      },
      {
        name: 'outputWarehouseOrderIndex',
        path: '/ss-oms/outputWarehouse/warehousing-order/index',
        component: () => import('@/views/oms/outputWarehouse/warehousingOrder/Index'),
        meta: {title: '出库单列表', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'outputWarehouseOrderShow',
        hidden: true,
        path: '/ss-oms/outputWarehouse/warehousing-order/show/:id',
        component: () => import('@/views/oms/outputWarehouse/warehousingOrder/Show'),
        meta: {title: '出库单详情', icon: 'list', noCache: true}
      },
      {
        name: 'outputWarehouseConfirmIndex',
        path: '/ss-oms/outputWarehouse/warehouse-confirm/index',
        component: () => import('@/views/oms/outputWarehouse/warehouseConfirm/Index'),
        meta: {title: '出库确认', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'outputWarehouseConfirmShow',
        hidden: true,
        path: '/ss-oms/outputWarehouse/warehouse-confirm/show/:id',
        component: () => import('@/views/oms/outputWarehouse/warehouseConfirm/Show'),
        meta: {title: '出库确认详情', icon: 'list', noCache: true}
      },
      {
        name: 'outputWarehousePicklistIndex',
        path: '/ss-oms/outputWarehouse/warehouse-picklist/index',
        component: () => import('@/views/oms/outputWarehouse/warehousePicklist/Index'),
        meta: {title: '出库拣货单', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'outputWarehousePicklistShow',
        hidden: true,
        path: '/ss-oms/outputWarehouse/warehouse-picklist/show/:id',
        component: () => import('@/views/oms/outputWarehouse/warehousePicklist/Show'),
        meta: {title: '出库拣货单-详情', icon: 'list'},
        alwaysShow: true
      },
      // 仓库配置
      {
        path: '/ss-oms/warehouse/configuration/index',
        name: 'warehouseConfigurationIndex',
        component: () => import('@/views/oms/warehouse/configuration/Index'),
        meta: {title: '仓库初始化配置', icon: 'list'},
        alwaysShow: true
      },
      {
        path: '/ss-oms/warehouse/configuration/edit/:id',
        name: 'warehouseConfigurationEdit',
        component: () => import('@/views/oms/warehouse/configuration/Edit'),
        meta: {title: '仓库初始化配置', icon: 'list'},
        hidden: true
      },
      {
        path: '/ss-oms/warehouse/transfer/index',
        name: 'transferIndex',
        component: () => import('@/views/oms/warehouse/transfer/Index'),
        meta: {title: '移库', icon: 'list'},
        alwaysShow: true
      },
      {
        path: '/ss-oms/warehouse/transfer/show/:id',
        name: 'transferShow',
        hidden: true,
        component: () => import('@/views/oms/warehouse/transfer/Show'),
        meta: {title: '移库详情', icon: 'list'}
      },
      {
        path: '/ss-oms/warehouse/transfer/add',
        name: 'transferAdd',
        hidden: true,
        component: () => import('@/views/oms/warehouse/transfer/Add'),
        meta: {title: '新增移库', icon: 'list'}
      },
      {
        path: '/ss-oms/warehouse/inventory/add',
        name: 'warehouseInventoryAdd',
        component: () => import('@/views/oms/warehouse/inventory/Add'),
        meta: {title: '新增盘库单', icon: 'list'},
        alwaysShow: true
      },
      {
        path: '/ss-oms/warehouse/inventory/index',
        name: 'warehouseInventoryIndex',
        component: () => import('@/views/oms/warehouse/inventory/Index'),
        meta: {title: '盘库列表', icon: 'list'},
        alwaysShow: true
      },
      {
        path: '/ss-oms/warehouse/inventory/register/:id',
        name: 'warehouseInventoryRegister',
        component: () => import('@/views/oms/warehouse/inventory/Register'),
        meta: {title: '登记盘库数据', icon: 'list'},
        hidden: true,
        alwaysShow: true
      },
      {
        path: '/ss-oms/warehouse/inventory/show/:id',
        name: 'warehouseInventoryShow',
        component: () => import('@/views/oms/warehouse/inventory/Show'),
        meta: {title: '盘点详情', icon: 'list'},
        hidden: true,
        alwaysShow: true
      },
      // 映射 发起报亏
      {
        path: '/ss-oms/warehouse/overflow/launch',
        name: 'warehouseOverflow',
        component: () => import('@/views/oms/warehouse/lossReport/OverflowLaunch'),
        meta: {title: '发起报溢单', icon: 'list'},
        alwaysShow: true
      },
      {
        path: '/ss-oms/warehouse/loss-report/launch',
        name: 'warehouseLossLaunch',
        component: () => import('@/views/oms/warehouse/lossReport/Launch'),
        meta: {title: '发起报亏单', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'lossReportIndex',
        path: '/ss-oms/warehouse/loss-report/index',
        component: () => import('@/views/oms/warehouse/lossReport/Index'),
        meta: {title: '报亏单列表', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'lossReportShow',
        hidden: true,
        path: '/ss-oms/warehouse/loss-report/show/:id',
        component: () => import('@/views/oms/warehouse/lossReport/Show'),
        meta: {title: '报亏单详情', icon: 'list', noCache: true}
      },
      {
        name: 'overflowIndex',
        path: '/ss-oms/warehouse/overflow/index',
        component: () => import('@/views/oms/warehouse/overflow/Index'),
        meta: {title: '报溢单列表', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'overflowShow',
        hidden: true,
        path: '/ss-oms/warehouse/overflow/show/:id',
        component: () => import('@/views/oms/warehouse/overflow/Show'),
        meta: {title: '报溢单详情', icon: 'list', noCache: true}
      },
      {
        name: 'tempStoreIndex',
        path: '/ss-oms/warehouse/temp-store/index',
        component: () => import('@/views/oms/warehouse/tempStore/Index'),
        meta: {title: '货主临时库存', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'tempStoreShow',
        hidden: true,
        path: '/ss-oms/warehouse/temp-store/show/:id',
        component: () => import('@/views/oms/warehouse/tempStore/Show'),
        meta: {title: '货主临时库存-详情', icon: 'list', noCache: true}
      },
      {
        path: '/ss-oms/warehouse/owner-longterm-storage/index',
        name: 'warehouseOwnerLongtermStorageIndex',
        component: () => import('@/views/oms/warehouse/ownerLongtermStorage/Index'),
        meta: {title: '货主长期仓储', icon: 'list'},
        alwaysShow: true
      },
      {
        path: '/ss-oms/warehouse/owner-longterm-storage/show/:id',
        name: 'warehouseOwnerLongtermStorageShow',
        component: () => import('@/views/oms/warehouse/ownerLongtermStorage/Show'),
        meta: {title: '货主长期仓储-详情', icon: 'list'},
        hidden: true,
        alwaysShow: true
      },
      {
        path: '/ss-oms/warehouse/record/OutWarehouse',
        name: 'warehouseRecordOutWarehouse',
        component: () => import('@/views/oms/warehouse/record/OutWarehouse'),
        meta: {title: '出库记录', icon: 'list'},
        alwaysShow: true
      },
      {
        path: '/ss-oms/warehouse/record/OutShow/:id',
        name: 'warehouseRecordOutShow',
        component: () => import('@/views/oms/warehouse/record/OutShow'),
        meta: {title: '出库记录-详情', icon: 'list'},
        hidden: true,
        alwaysShow: true
      },
      {
        path: '/ss-oms/warehouse/record/EnterWarehouse',
        name: 'warehouseRecordEnterWarehouse',
        component: () => import('@/views/oms/warehouse/record/EnterWarehouse'),
        meta: {title: '入库记录', icon: 'list'},
        alwaysShow: true
      },
      {
        path: '/ss-oms/warehouse/record/EnterShow/:id',
        name: 'warehouseRecordEnterShow',
        component: () => import('@/views/oms/warehouse/record/EnterShow'),
        meta: {title: '入库记录-详情', icon: 'list'},
        hidden: true,
        alwaysShow: true
      },
      {
        name: 'printPickIndex',
        path: '/ss-oms/print/pick/index',
        component: () => import('@/views/oms/print/pick/Index'),
        meta: {title: '提货装车单', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'printLabelIndex',
        path: '/ss-oms/print/label/index',
        component: () => import('@/views/oms/print/label/Index'),
        meta: {title: '标签', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'printLabelShow',
        path: '/ss-oms/print/label/show/:id',
        component: () => import('@/views/oms/print/label/Show'),
        meta: {title: '标签-打印详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'printDistributionIndex',
        path: '/ss-oms/print/distribution/index',
        component: () => import('@/views/oms/print/distribution/Index'),
        meta: {title: '配送装车单', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'printTransferIndex',
        path: '/ss-oms/print/two-grade-transfer/index',
        component: () => import('@/views/oms/print/transfer/Index'),
        meta: {title: '干线装车单', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'dispatchDistributionOrderIndex',
        path: '/ss-oms/dispatch/distribution-order/index',
        component: () => import('@/views/oms/dispatch/distributionOrder/Index'),
        meta: {title: '配装服务单预约', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'appointIndex',
        path: '/ss-oms/appoint/index',
        component: () => import('@/views/oms/appoint/Index'),
        meta: {title: '用户自提预约', icon: 'list'},
        alwaysShow: true
      },
      {
        path: '/ss-oms/appoint/order-detail/:id',
        name: 'appointOrderDetail',
        hidden: true,
        component: () => import('@/views/oms/orders/serviceOrders/Show'),
        meta: {title: '订单详情', icon: 'list', noCache: true}
      },
      // 映射‘配送调度’组件
      // {
      //   name: 'loadingDispatchIndex',
      //   path: '/ss-oms/dispatch/loading/index',
      //   component: () => import('@/views/oms/dispatch/distributionDispatch/LoadingIndex'),
      //   meta: {title: '配装一体调度', icon: 'list'},
      //   alwaysShow: true
      // },
      // {
      //   name: 'distributionYtOrderDetail',
      //   path: '/ss-oms/dispatch/loading/order-detail/:id',
      //   component: () => import('@/views/oms/orders/serviceOrders/Show'),
      //   meta: {title: '配装一体调度-订单详情', icon: 'list'},
      //   hidden: true,
      //   alwaysShow: true
      // },
      // {
      //   name: 'distributionDispatchIndex',
      //   path: '/ss-oms/dispatch/distribution/index',
      //   component: () => import('@/views/oms/dispatch/distributionDispatch/Index'),
      //   meta: {title: '配送调度', icon: 'list'},
      //   alwaysShow: true
      // },
      // {
      //   name: 'distributionPsOrderDetail',
      //   path: '/ss-oms/dispatch/distribution/order-detail/:id',
      //   component: () => import('@/views/oms/orders/serviceOrders/Show'),
      //   meta: {title: '配送调度-订单详情', icon: 'list'},
      //   hidden: true,
      //   alwaysShow: true
      // },
      // 映射‘配送调度’组件
      {
        name: 'installDispatchIndex',
        path: '/ss-oms/dispatch/install/index',
        component: () => import('@/views/oms/dispatch/distributionDispatch/InstallIndex'),
        meta: {title: '服务调度', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'installOrderDetail',
        path: '/ss-oms/dispatch/install/order-detail/:id',
        component: () => import('@/views/oms/orders/serviceOrders/Show'),
        meta: {title: '服务调度-订单详情', icon: 'list'},
        hidden: true,
        alwaysShow: true
      },
      {
        name: 'deliveryDispatchIndex',
        path: '/ss-oms/dispatch/delivery/index',
        component: () => import('@/views/oms/dispatch/deliveryDispatch/Index'),
        meta: {title: '提货调度', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'deliveryDispatchOrderDetail',
        path: '/ss-oms/dispatch/delivery/order-detail/:id',
        component: () => import('@/views/oms/orders/serviceOrders/Show'),
        meta: {title: '提货调度-订单详情', icon: 'list'},
        hidden: true,
        alwaysShow: true
      },
      {
        name: 'twoGradeTransferDispatchIndex',
        path: '/ss-oms/dispatch/two-grade-transfer-dispatch/index',
        component: () => import('@/views/oms/dispatch/twoGradeTransferDispatch/Index'),
        meta: {title: '干线调度', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'twoGradeTransferDispatchOrderDetail',
        path: '/ss-oms/dispatch/two-grade-transfer-dispatch/order-detail/:id',
        component: () => import('@/views/oms/orders/serviceOrders/Show'),
        meta: {title: '干线调度-订单详情', icon: 'list'},
        hidden: true,
        alwaysShow: true
      },
      {
        name: 'myWalletIndex',
        path: '/ss-oms/myWallet/index',
        component: () => import('@/views/oms/myWallet/Index'),
        meta: {title: '我的钱包', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'myWalletSetPsdShow',
        path: '/ss-oms/myWallet/setPsdShow',
        component: () => import('@/views/oms/myWallet/SetPsdShow'),
        meta: {title: '钱包密码', icon: 'list', noCache: true},
        hidden: true
      },
      {
        name: 'logListIndex',
        path: '/ss-oms/my-wallet/log-list',
        component: () => import('@/views/oms/myWallet/LogList'),
        meta: {title: '账单流水列表', icon: 'list'},
        hidden: true
      },
      {
        name: 'bankCardIndex',
        path: '/ss-oms/myWallet/bankCardIndex',
        component: () => import('@/views/oms/myWallet/BankCardIndex'),
        meta: {title: '银行卡列表', icon: 'list', noCache: true},
        hidden: true
      },
      {
        name: 'addBankCard',
        path: '/ss-oms/myWallet/addBankCard',
        component: () => import('@/views/oms/myWallet/AddBankCard'),
        meta: {title: '添加银行卡', icon: 'list', noCache: true},
        hidden: true
      },
      {
        name: 'resourceDistributionInstall',
        path: '/ss-oms/resource/distributionInstall/index',
        component: () => import('@/views/oms/resource/distributionInstall/Index'),
        meta: {title: '配安资源管理', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'resourcePick',
        path: '/ss-oms/resource/pick/index',
        component: () => import('@/views/oms/resource/pick/Index'),
        meta: {title: '拣货资源管理', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'resourcePickup',
        path: '/ss-oms/resource/pickup/index',
        component: () => import('@/views/oms/resource/pickup/Index'),
        meta: {title: '提货资源管理', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'resourceSecondary',
        path: '/ss-oms/resource/secondary/index',
        component: () => import('@/views/oms/resource/secondary/Index'),
        meta: {title: '干线资源管理', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'workerInfo',
        path: '/ss-oms/resource/common/show/:id',
        component: () => import('@/views/oms/resource/common/Show'),
        meta: {title: '资源详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'basicDataManufactorProIndex',
        path: '/ss-oms/basic-data/manufactor-pro/index',
        component: () => import('@/views/oms/basicData/manufactorPro/Index'),
        meta: {title: '厂家产品', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'basicDataManufactorProShow',
        path: '/ss-oms/basic-data/manufactor-pro/index-pro/:id',
        component: () => import('@/views/oms/basicData/manufactorPro/IndexPro'),
        meta: {title: '厂家产品详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'basicDataProductionIndex',
        path: '/ss-oms/basic-data/production/index',
        component: () => import('@/views/oms/basicData/production/Index'),
        meta: {title: '商家产品', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'basicDataProductionShow',
        path: '/ss-oms/basic-data/production/show/:id',
        component: () => import('@/views/oms/basicData/production/Show'),
        meta: {title: '商家产品详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'basicDataOwnerIndex',
        path: '/ss-oms/basic-data/owner/index',
        component: () => import('@/views/oms/basicData/owner/Index'),
        meta: {title: '商家', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'basicDataOwnerCreate',
        path: '/ss-oms/basic-data/owner/create',
        component: () => import('@/views/oms/basicData/owner/Create'),
        meta: {title: '商家新增', icon: 'list'},
        hidden: true
      },
      {
        name: 'basicDataOwnerEdit',
        path: '/ss-oms/basic-data/owner/edit/:id',
        component: () => import('@/views/oms/basicData/owner/Create'),
        meta: {title: '商家编辑', icon: 'list'},
        hidden: true
      },
      {
        name: 'basicDataOwnerShow',
        path: '/ss-oms/basic-data/owner/show/:id',
        component: () => import('@/views/oms/basicData/owner/Show'),
        meta: {title: '商家详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'basicDataManufactorIndex',
        path: '/ss-oms/basic-data/manufactor/index',
        component: () => import('@/views/oms/basicData/manufactor/Index'),
        meta: {title: '厂家', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'basicDataManufactorCreate',
        path: '/ss-oms/basic-data/manufactor/create',
        component: () => import('@/views/oms/basicData/manufactor/Create'),
        meta: {title: '厂家新增', icon: 'list'},
        hidden: true
      },
      {
        name: 'basicDataManufactorEdit',
        path: '/ss-oms/basic-data/manufactor/edit/:id',
        component: () => import('@/views/oms/basicData/manufactor/Create'),
        meta: {title: '厂家编辑', icon: 'list'},
        hidden: true
      },
      {
        name: 'basicDataManufactorShow',
        path: '/ss-oms/basic-data/manufactor/show/:id',
        component: () => import('@/views/oms/basicData/manufactor/Show'),
        meta: {title: '厂家详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'userMy',
        path: '/ss-oms/user/my',
        component: () => import('@/views/oms/user/My'),
        meta: {title: '我的账号', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'basicDataCompanyMy',
        path: '/ss-oms/basic-data/company/my',
        component: () => import('@/views/oms/basicData/company/My'),
        meta: {title: '我的公司', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'userIndex',
        path: '/ss-oms/user/index',
        component: () => import('@/views/oms/user/Index'),
        meta: {title: '账号管理', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'userAdd',
        path: '/ss-oms/user/create',
        component: () => import('@/views/oms/user/Create'),
        meta: {title: '账号新增', icon: 'list'},
        hidden: true
      },
      {
        name: 'userEdit',
        path: '/ss-oms/user/create/:id',
        component: () => import('@/views/oms/user/Create'),
        meta: {title: '账号修改', icon: 'list'},
        hidden: true
      },
      {
        name: 'userShow',
        path: '/ss-oms/user/show/:id',
        component: () => import('@/views/oms/user/Show'),
        meta: {title: '账号详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'basicDataCompanyIndex',
        path: '/ss-oms/basic-data/company/index',
        component: () => import('@/views/oms/basicData/company/Index'),
        meta: {title: '公司管理', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'basicDataCompanyShow',
        path: '/ss-oms/basic-data/company/show/:id',
        component: () => import('@/views/oms/basicData/company/Show'),
        meta: {title: '公司详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'basicDataCompanyCreate',
        path: '/ss-oms/basic-data/company/create',
        component: () => import('@/views/oms/basicData/company/Create'),
        meta: {title: '公司新增', icon: 'list'},
        hidden: true
      },
      {
        name: 'basicDataCompanyEdit',
        path: '/ss-oms/basic-data/company/edit/:id',
        component: () => import('@/views/oms/basicData/company/Create'),
        meta: {title: '公司修改', icon: 'list'},
        hidden: true
      },
      // 系统管理用的 账号列表
      // 映射到：账号管理（组件）
      {
        path: '/ss-oms/info/user/index',
        name: 'infoUserIndex',
        component: () => import('@/views/oms/user/InfoIndex'),
        meta: {title: '账号列表', icon: 'list'},
        alwaysShow: true
      },
      // 系统 账号详情
      {
        path: '/ss-oms/info/user/show/:id',
        name: 'infoUserShow',
        component: () => import('@/views/oms/user/Show'),
        meta: {title: '账号详情', icon: 'list'},
        hidden: true
      },
      // 系统 账号修改
      {
        path: '/ss-oms/info/user/create/:id',
        name: 'infoUserEdit',
        component: () => import('@/views/oms/user/Create'),
        meta: {title: '账号修改', icon: 'list'},
        hidden: true
      },
      // 系统 账号新增
      {
        path: '/ss-oms/info/user/create',
        name: 'infoUserAdd',
        component: () => import('@/views/oms/user/Create'),
        meta: {title: '账号新增', icon: 'list'},
        hidden: true
      },
      {
        name: 'receivablesIndex',
        path: '/ss-oms/orders/receivables/index',
        component: () => import('@/views/oms/orders/receivables/Index'),
        meta: {title: '代收款订单', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'cashExamineIndex',
        path: '/ss-oms/information/cash-examine/index',
        component: () => import('@/views/oms/information/cashExamine/Index'),
        meta: {title: '提现审核', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'informationMenu',
        path: '/ss-oms/information/menu',
        component: () => import('@/views/oms/information/Menu'),
        meta: {title: '菜单管理', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'roleIndex',
        path: '/ss-oms/information/role',
        component: () => import('@/views/oms/information/Role'),
        meta: {title: '角色管理', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'roleShow',
        path: '/ss-oms/information/role-show/:id',
        component: () => import('@/views/oms/information/RoleShow'),
        meta: {title: '角色详情', icon: 'list', noCache: true},
        hidden: true
      },
      {
        name: 'roleCreate',
        path: '/ss-oms/information/role-show',
        component: () => import('@/views/oms/information/RoleShow'),
        meta: {title: '新增角色', icon: 'list', noCache: true},
        hidden: true
      },
      {
        name: 'abnormalLaunchIndex',
        path: '/ss-oms/abnormal/launch/index',
        component: () => import('@/views/oms/abnormal/launch/Index'),
        meta: {title: '异常发起', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'abnormalIndex',
        path: '/ss-oms/abnormal/list/index',
        component: () => import('@/views/oms/abnormal/list/Index'),
        meta: {title: '仓库异常', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'abnormalShow',
        path: '/ss-oms/abnormal/list/show/:id',
        component: () => import('@/views/oms/abnormal/list/Show'),
        meta: {title: '仓库异常-详情', icon: 'list', noCache: true}
      },
      {
        name: 'abnormalServiceAbnormalIndex',
        path: '/ss-oms/service/abnormal/index',
        component: () => import('@/views/oms/abnormal/serviceAbnormal/Index'),
        meta: {title: '服务异常', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'abnormalServiceAbnormalShow',
        path: '/ss-oms/service/abnormal/show/:id',
        component: () => import('@/views/oms/abnormal/serviceAbnormal/Show'),
        meta: {title: '服务异常详情', icon: 'list', noCache: true},
        alwaysShow: true
      },
      {
        name: 'expatriateOrdersIndex',
        path: '/ss-oms/orders/expatriate/expatriateOrders/index',
        component: () => import('@/views/oms/orders/expatriate/expatriateOrders/Index'),
        meta: {title: '抛出订单', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'expatriateOrdersShow',
        path: '/ss-oms/orders/expatriate/expatriateOrders/show/:id',
        component: () => import('@/views/oms/orders/expatriate/expatriateOrders/Show'),
        meta: {title: '抛出订单详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'expatriateOfferingIndex',
        path: '/ss-oms/orders/expatriate/offering/index',
        component: () => import('@/views/oms/orders/expatriate/offering/Index'),
        meta: {title: '报价中', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'expatriateOfferingShow',
        path: '/ss-oms/orders/expatriate/offering/show/:id',
        component: () => import('@/views/oms/orders/expatriate/offering/Show'),
        meta: {title: '报价中详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'expatriatePendingIndex',
        path: '/ss-oms/orders/expatriate/pending/index',
        component: () => import('@/views/oms/orders/expatriate/pending/Index'),
        meta: {title: '待付款', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'expatriatePendingShow',
        path: '/ss-oms/orders/expatriate/pending/show/:id',
        component: () => import('@/views/oms/orders/expatriate/pending/Show'),
        meta: {title: '待付款详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'expatriateInServiceIndex',
        path: '/ss-oms/orders/expatriate/inService/index',
        component: () => import('@/views/oms/orders/expatriate/inService/Index'),
        meta: {title: '服务中', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'expatriateInServiceShow',
        path: '/ss-oms/orders/expatriate/inService/show/:id',
        component: () => import('@/views/oms/orders/expatriate/inService/Show'),
        meta: {title: '服务中详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'expatriateConfirmingIndex',
        path: '/ss-oms/orders/expatriate/confirming/index',
        component: () => import('@/views/oms/orders/expatriate/confirming/Index'),
        meta: {title: '待确认', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'expatriateConfirmingShow',
        path: '/ss-oms/orders/expatriate/confirming/show/:id',
        component: () => import('@/views/oms/orders/expatriate/confirming/Show'),
        meta: {title: '待确认详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'expatriateCompletedIndex',
        path: '/ss-oms/orders/expatriate/completed/index',
        component: () => import('@/views/oms/orders/expatriate/completed/Index'),
        meta: {title: '已完成', icon: 'list'},
        alwaysShow: true
      },
      {
        name: 'expatriateCompletedShow',
        path: '/ss-oms/orders/expatriate/completed/show/:id',
        component: () => import('@/views/oms/orders/expatriate/completed/Show'),
        meta: {title: '已完成详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'expatriateOfferingPayment',
        path: '/ss-oms/orders/expatriate/offering/payment/:id',
        component: () => import('@/views/oms/orders/expatriate/Payment'),
        meta: {title: '支付', icon: 'list'},
        hidden: true
      },
      {
        name: 'expatriatePendingPayment',
        path: '/ss-oms/orders/expatriate/pending/payment/:id',
        component: () => import('@/views/oms/orders/expatriate/Payment'),
        meta: {title: '支付', icon: 'list'},
        hidden: true
      },
      {
        name: 'expatriateInServicePaymentAdditional',
        path: '/ss-oms/orders/expatriate/inService/paymentAdditional/:id',
        component: () => import('@/views/oms/orders/expatriate/PaymentAdditional'),
        meta: {title: '追加费用', icon: 'list'},
        hidden: true
      },
      {
        name: 'expatriateConfirmingPaymentAdditional',
        path: '/ss-oms/orders/expatriate/confirming/paymentAdditional/:id',
        component: () => import('@/views/oms/orders/expatriate/PaymentAdditional'),
        meta: {title: '追加费用', icon: 'list'},
        hidden: true
      },
      {
        name: 'shopShow',
        path: '/ss-oms/shop/show',
        component: () => import('@/views/oms/shop/Show'),
        meta: {title: '商城基础资料', icon: 'list', noCache: true}
      },
      {
        name: 'shopStore',
        path: '/ss-oms/shop/store',
        component: () => import('@/views/oms/shop/Store'),
        meta: {title: '体验门店管理', icon: 'list'}
      },
      {
        name: 'shopGoods',
        path: '/ss-oms/shop/goods',
        component: () => import('@/views/oms/shop/Goods'),
        meta: {title: '商品管理', icon: 'list'}
      },
      {
        name: 'shopGoodsCreate',
        path: '/ss-oms/shop/goods/create',
        component: () => import('@/views/oms/shop/GoodsCreate'),
        meta: {title: '新增商品', icon: 'list'}
      },
      {
        name: 'shopGoodsEdit',
        path: '/ss-oms/shop/goods/edit/:id',
        component: () => import('@/views/oms/shop/GoodsCreate'),
        meta: {title: '编辑商品', icon: 'list'}
      },
      {
        name: 'shopTrade',
        path: '/ss-oms/shop/trade',
        component: () => import('@/views/oms/shop/Trade'),
        meta: {title: '交易管理', icon: 'list'}
      },
      {
        name: 'shopTradeCustomer',
        path: '/ss-oms/shop/tradecustomer',
        component: () => import('@/views/oms/shop/TradeCustomer'),
        meta: {title: '交易管理(客服)', icon: 'list'}
      },
      {
        name: 'shopDelivery',
        path: '/ss-oms/shop/delivery/:id',
        component: () => import('@/views/oms/order/CreateService'),
        meta: {title: '确认发货', icon: 'list'}
      },
      {
        name: 'trunkLine',
        path: '/ss-oms/trunkLine',
        component: () => import('@/views/oms/trunkLine/Index'),
        meta: {title: '线路管理', icon: 'list'}
      },
      {
        name: 'trunkLineIndex',
        path: '/ss-oms/trunkLine/index',
        component: () => import('@/views/oms/trunkLine/Index'),
        meta: {title: '线路管理', icon: 'list'}
      },
      {
        name: 'compensatePromiseIndex',
        path: '/ss-oms/compensatePromise/index',
        component: () => import('@/views/oms/compensatePromise/Index'),
        meta: {title: '赔付承诺', icon: 'list'}
      },
      {
        name: 'trunkLineShow',
        path: '/ss-oms/trunkLine/show/:id',
        component: () => import('@/views/oms/trunkLine/Show'),
        meta: {title: '线路详情', icon: 'list'},
        hidden: true
      },
      {
        name: 'trunkLineEdit',
        path: '/ss-oms/trunkLine/edit',
        component: () => import('@/views/oms/trunkLine/Edit'),
        meta: {title: '线路编辑', icon: 'list'},
        hidden: true
      },
      {
        name: 'approve',
        path: '/ss-oms/approve',
        component: () => import('@/views/oms/approve/Index'),
        meta: {title: '审核管理', icon: 'list'}
      },
      {
        name: 'approveIndex',
        path: '/ss-oms/approve/index',
        component: () => import('@/views/oms/approve/Index'),
        meta: {title: '资质管理', icon: 'list'}
      },
      {
        name: 'approveDetail',
        path: '/ss-oms/approve/detail/:id',
        component: () => import('@/views/oms/approve/Detail'),
        meta: {title: '资质详情', icon: 'list'},
        hidden: true
      },
      // 干线-订单查询
      // {
      //   name: 'trunkOrderIndex',
      //   path: '/ss-oms/trunkOrder/index',
      //   component: () => import('@/views/oms/trunkOrder/Index'),
      //   meta: {title: '订单查询', icon: 'list'},
      //   alwaysShow: true
      // },
      // 干线-待揽收列表
      {
        name: 'trunkOrderCollectIndex',
        path: '/ss-oms/trunkOrder/collect/index',
        component: () => import('@/views/oms/trunkOrder/collect/Index'),
        meta: {title: '待揽收订单', icon: 'list'},
        alwaysShow: true
      },
      // 提货地址管理 -列表
      {
        name: 'pickAddressIndex',
        path: '/ss-oms/pickAddress/index',
        component: () => import('@/views/oms/pickAddress/Index'),
        meta: {title: '提货地址管理', icon: 'list'},
        alwaysShow: true
      }
    ]
  },
  {
    path: '*',
    component: () => import('@/views/errorPage/404'),
    hidden: true
  }
]

export default new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({y: 0}),
  routes: constantRouterMap
})
