/**
 * Created by jiachenpan on 16/11/18.
 */
import validatorUtil from './validatorUtil'
import {isEqual} from './index'
export {isvalidUsername, validateURL} from './validatorUtil'
// 是否是手机
export function isPhone(rule, value, callback) {
  if (validatorUtil.isPhoneUtil(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || '手机号格式有误'))
}

// 是否是链接
export function validateHttp(rule, value, callback) {
  if (validatorUtil.validateURL(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || '链接格式有误'))
}

// 手机号
export function isPhoneAlias(rule, value, callback) {
  if (validatorUtil.isPhoneAlias(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || '手机号格式有误'))
}

export function isPhoneOrTel(rule, value, callback) {
  if (validatorUtil.isPhoneOrTelUtil(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || '电话格式有误'))
}

// 电话
export function isTel(rule, value, callback) {
  if (validatorUtil.isTel(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || '电话格式有误'))
}

// 是一个地址
export function isCascadeAddress(rule, value, callback) {
  if (Array.isArray(value) && value.length === 0) {
    callback(new Error('地址必须'))
    return
  } else if (Array.isArray(value) && value.length > 0) {
    callback()
    return
  }
  callback(new Error('不是一个有效的地址'))
}

// 整数验证
export function isInteger(rule, value, callback) {
  if (validatorUtil.isIntegerStringUtil(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '不是一个整型'))
}

// 范围验证（验证字符长度范围和数字最大最小值）
export function range(rule, value, callback) {
  let isString = rule.isString
  if (validatorUtil.rangeUtil(value, rule.min || 0, rule.max || undefined, isString)) {
    callback()
    return
  }
  let message = rule.message || ''
  message = message.replace('{min}', rule.min || 0).replace('{max}', rule.max || 0)
  callback(new Error(message || rule.field + '范围不满足'))
}

// 小数验证
export function isNumber(rule, value, callback) {
  if (validatorUtil.isNumberStringUtil(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '不是一个数字'))
}

// 规则的小数 (限制了整数位和小数位)
export function isFormatNumber(rule, value, callback) {
  if (validatorUtil.isFormatNumberStringUtil(value, rule.decimal || 2, rule.int)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '不是一个规则的小数'))
}

// select 必须
export function selectRequired(rule, value, callback) {
  if (value && value.value) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '必须'))
}

// select 必须
export function selectRequiredIf(rule, value, callback) {
  const ifFunc = validatorUtil.isFunction(rule.if) ? rule.if : () => {
    return true
  }
  if (!ifFunc() || (value && value.value)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '必须'))
}

// arrayRequired
export function arrayRequired(rule, value, callback) {
  const minLength = rule.min
  let minLen = 1
  if (minLength) {
    minLen = minLength
  }
  if (Array.isArray(value) && value.length >= minLen) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '必须'))
}

// 地址验证
export function addressRequired(rule, value, callback) {
  const minLength = rule.min
  let minLen = 1
  if (minLength) {
    minLen = minLength
  }
  if (Array.isArray(value) && value.length >= minLen) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '必须'))
}

// 满足条件必须
export function requiredIf(rule, value, callback) {
  const ifFunc = validatorUtil.isFunction(rule.if) ? rule.if : () => {
    return true
  }
  if (!ifFunc() || !validatorUtil.empty(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '必须'))
}

// 满足条件必须
export function required(rule, value, callback) {
  if (!validatorUtil.empty(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '必须'))
}

// 输入 (中文、英文)
export function isText(rule, value, callback) {
  if (validatorUtil.isTextUtil(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '必须'))
}

// 备注输入限制
export function isMaxLength(rule, value, callback) {
  if (!value || (value && value.length <= 200)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '必须'))
}

// 判断不能有空白字符 (空格符、制表符、回车符、换行符、垂直换行符、换页符、、)
export function eliminateSpace(rule, value, callback) {
  if (validatorUtil.eliminateSpaceUtil(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '不能有空白字符'))
}

// 判断只能中文
export function isChinese(rule, value, callback) {
  if (validatorUtil.isChinese(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '必须是汉字'))
}

// 判断只能英文
export function isEnglish(rule, value, callback) {
  if (validatorUtil.isEnglish(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '必须是英文'))
}

// 判断只能字母或数字
export function isEnglishOrNumber(rule, value, callback) {
  if (validatorUtil.isEnglishOrNumber(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '必须是英文'))
}

// 判断只能 (中文、英文、数字)
export function isTextEnglish(rule, value, callback) {
  if (validatorUtil.isChinese(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '必须是汉字'))
}

// 判断只能 (中文、英文、数字)
export function isTextEnglishWithEspecial(rule, value, callback) {
  if (validatorUtil.isTextEnglishWithEspecialUtil(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '必须是汉字'))
}

// 判断非中文
export function isNotChinese(rule, value, callback) {
  if (validatorUtil.isNotChinese(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '不能是汉字'))
}

// 判断是否是身份证
export function isId(rule, value, callback) {
  if (validatorUtil.isID(value)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '必须是身份证'))
}

// 判断和其他值 是否相等
export function isSame(rule, value, callback) {
  const otherValue = rule.otherValue
  if (isEqual(otherValue, value)) {
    callback()
    return
  }
  callback(new Error(rule.message || rule.field + '必须相同'))
}

// 正则验证
export function regexValidator(rule, value, callback) {
  let regex = validatorUtil.isRegExp(rule.regex) ? validatorUtil.isRegExp(rule.regex) : new RegExp(rule.regex)
  const valueHandle = rule.valueHandle
  switch (valueHandle) {
    case 'stringify':
      value = value ? JSON.stringify(value) : value
      break
    case 'join':
      value = Array.isArray(value) ? value.join(',') : value
      break
  }
  if (valueHandle === 'stringify') {
    console.log(regex.test(value), value)
  }
  if (regex.test(value)) {
    callback()
  }
  callback(new Error(rule.message || '正则验证未通过'))
}

export default {
  isPhone,
  isPhoneAlias,
  isTel,
  isCascadeAddress,
  isInteger,
  range,
  isNumber,
  isFormatNumber,
  selectRequired,
  arrayRequired,
  requiredIf,
  isText,
  isTextEnglish,
  isChinese,
  isEnglish,
  isMaxLength,
  eliminateSpace,
  isTextEnglishWithEspecial,
  isPhoneOrTel,
  isNotChinese
}
