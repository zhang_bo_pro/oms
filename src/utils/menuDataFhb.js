export default [
  // 我的账号
  {id: 1, pid: 0, icon: 'm-menu__link-icon iconfont icon-shouye', path: '/', text: '首页'},
  {id: 12, pid: 11, icon: 'fa fa-edit', path: '/ss-fhb/myAccount/changePsw/:id', text: '我的账号-修改密码'},
  {id: 13, pid: 11, icon: 'fa fa-edit', path: '/ss-fhb/myAccount/change-phone/:phone', text: '我的账号-改绑手机'},
  {id: 20, pid: 0, icon: 'm-menu__link-icon iconfont icon-xiadan', path: '/ss-fhb/fahuobao', text: '我要下单'},
  {id: 21, pid: 20, icon: 'fa fa-edit', path: '/ss-fhb/fa-temporary/index', text: '订单池'},
  {id: 22, pid: 21, icon: 'fa fa-edit', path: '/ss-fhb/fa-temporary/show/:id', text: '订单池-详情'},
  {id: 23, pid: 20, icon: 'fa fa-edit', path: '/ss-fhb/fa-temporary/show', text: '录单'},
  // 企业认证（商家信息）
  {id: 30, pid: 0, icon: 'm-menu__link-icon iconfont icon-qiyerenzheng', path: '/ss-fhb/companyCertification', text: '企业认证'},
  {id: 31, pid: 30, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-fhb/companyCertification/index', text: '企业认证'},
  {id: 32, pid: 30, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-fhb/companyCertification/edit/:businessId', text: '去认证'},
  {id: 33, pid: 30, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-fhb/companyCertification/show/:id', text: '认证详情'},
  // 提货地址管理
  {id: 35, pid: 0, icon: 'm-menu__link-icon iconfont icon-dizhiguanli', path: '/ss-fhb/pickAddress', text: '提货地址管理'},
  {id: 36, pid: 35, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-fhb/pickAddress/index', text: '提货地址管理'},
  // 账号管理
  {id: 40, pid: 0, icon: 'm-menu__link-icon iconfont icon-xitong', path: '/ss-fhb/account', text: '账号管理'},
  {id: '40-1', pid: 40, icon: 'fa fa-edit', path: '/ss-fhb/myAccount/index', text: '我的账号'},
  {id: 41, pid: 40, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-fhb/account/index', text: '员工管理'},
  {id: 42, pid: 41, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-fhb/account/create', text: '新增员工账号', hidden: 1},
  {id: 43, pid: 41, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-fhb/account/show/:id', text: '员工账号详情'},
  {id: 44, pid: 43, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-fhb/account/edit/:id', text: '修改员工账号'},
  // 我的师傅
  {id: 50, pid: 0, icon: 'm-menu__link-icon iconfont icon-ren', path: '/ss-fhb/myMaster/index', text: '我的师傅'},
  // 我的钱包
  {id: 60, pid: 0, icon: 'm-menu__link-icon iconfont icon-daifukuan', path: '/ss-fhb/myWallet', text: '我的钱包'},
  {id: 61, pid: 60, icon: 'fa fa-edit', path: '/ss-fhb/myWallet/index', text: '我的钱包'},
  {id: 62, pid: 61, icon: 'fa fa-edit', path: '/ss-fhb/myWallet/setPsdShow', text: '钱包密码', hidden: 1},
  {id: 63, pid: 61, icon: 'fa fa-edit', path: '/ss-fhb/myWallet/recharge/:money/:totalMoney', text: '钱包充值', hidden: 1},
  {id: 64, pid: 60, icon: 'fa fa-icon fa-edit', path: '/ss-fhb/myWallet/bankCardIndex', text: '钱包银行卡', hidden: 1},
  {id: 65, pid: 64, icon: 'fa fa-edit', path: '/ss-fhb/myWallet/addBankCard', text: '添加银行卡', hidden: 1},
  // 账单-列表页
  {id: 66, pid: 61, icon: 'fa fa-edit', path: '/ss-fhb/my-wallet/log-list', text: '我的账单', hidden: 1},
  {id: 67, pid: 61, icon: 'fa fa-edit', path: '/ss-fhb/my-wallet/couponList', text: '优惠券列表', hidden: 1},
  {id: 68, pid: 67, icon: 'fa fa-edit', path: '/ss-fhb/my-wallet/coupon-show/:id', text: '优惠券-详情', hidden: 1},
  {id: 69, pid: 61, icon: 'fa fa-edit', path: '/ss-fhb/myWallet/recharge/:inputMoney', text: '钱包充值'},
  // 我订单
  {id: 100, pid: 0, icon: 'm-menu__link-icon iconfont icon-dingdan2', path: '/ss-fhb/myOrders', text: '我的订单'},
  {id: 101, pid: 100, icon: 'fa fa-edit', path: '/ss-fhb/myOrders/offering/index', text: '抢单中'},
  {id: 102, pid: 101, icon: 'fa fa-edit', path: '/ss-fhb/myOrders/offering/show/:id', text: '订单详情'},
  {id: 103, pid: 100, icon: 'fa fa-edit', path: '/ss-fhb/myOrders/pending/index', text: '待付款'},
  {id: 104, pid: 103, icon: 'fa fa-edit', path: '/ss-fhb/myOrders/pending/show/:id', text: '订单详情'},
  {id: 105, pid: 100, icon: 'fa fa-edit', path: '/ss-fhb/myOrders/inService/index', text: '服务中'},
  {id: 106, pid: 105, icon: 'fa fa-edit', path: '/ss-fhb/myOrders/inService/show/:id', text: '订单详情'},
  {id: 107, pid: 100, icon: 'fa fa-edit', path: '/ss-fhb/myOrders/confirming/index', text: '待确认'},
  {id: 108, pid: 107, icon: 'fa fa-edit', path: '/ss-fhb/myOrders/confirming/show/:id', text: '订单详情'},
  {id: 109, pid: 100, icon: 'fa fa-edit', path: '/ss-fhb/myOrders/completed/index', text: '已完成'},
  {id: 110, pid: 109, icon: 'fa fa-edit', path: '/ss-fhb/myOrders/completed/show/:id', text: '订单详情'},
  {id: 111, pid: 100, icon: 'fa fa-edit', path: '/ss-fhb/myOrders/offering/payment/:id', text: '支付'},
  {id: 112, pid: 100, icon: 'fa fa-edit', path: '/ss-fhb/myOrders/pending/payment/:id', text: '支付'},
  {id: 113, pid: 100, icon: 'fa fa-edit', path: '/ss-fhb/myOrders/inService/paymentAdditional/:id', text: '追加费用'},
  {id: 114, pid: 100, icon: 'fa fa-edit', path: '/ss-fhb/myOrders/confirming/paymentAdditional/:id', text: '追加费用'},
  {id: 115, pid: 100, icon: 'fa fa-edit', path: '/ss-fhb/myOrders/mergePay/:ids', text: '合并支付'},
  // 订单查询
  {id: 120, pid: 0, icon: 'm-menu__link-icon iconfont icon-chaxunyewu', path: '/ss-fhb/order/index', text: '订单查询'},
  {id: 121, pid: 120, icon: 'fa fa-edit', path: '/ss-fhb/order/show/:id', text: '订单详情'},

  // 支付审批
  {id: 130, pid: 0, icon: 'm-menu__link-icon iconfont icon-approval', path: '/ss-fhb/paymentApprove', text: '支付审批'},
  {id: 131, pid: 130, icon: 'fa fa-edit', path: '/ss-fhb/paymentApprove/pending/index', text: '待发起'},
  {id: 132, pid: 131, icon: 'fa fa-edit', path: '/ss-fhb/paymentApprove/pending/show/:id', text: '待发起详情'},
  {id: 133, pid: 130, icon: 'fa fa-edit', path: '/ss-fhb/paymentApprove/approve/index', text: '待审批'},
  {id: 134, pid: 133, icon: 'fa fa-edit', path: '/ss-fhb/paymentApprove/approve/show/:id', text: '待审批详情'},
  {id: 135, pid: 130, icon: 'fa fa-edit', path: '/ss-fhb/paymentApprove/payment/index', text: '待支付'},
  {id: 136, pid: 135, icon: 'fa fa-edit', path: '/ss-fhb/paymentApprove/payment/show/:id', text: '待支付详情'},
  {id: 137, pid: 135, icon: 'fa fa-edit', path: '/ss-fhb/paymentApprove/payment/payment/:id', text: '支付'},
  {id: 138, pid: 135, icon: 'fa fa-edit', path: '/ss-fhb/paymentApprove/payment/alreadyPay/:id', text: '已支付详情'},

  {id: 200, pid: 0, icon: 'm-menu__link-icon iconfont icon-kefu1', path: '/ss-fhb/myOrders/after-service', text: '售后服务'},
  {id: 201, pid: 200, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-fhb/complaint', text: '投诉管理'},
  {id: 202, pid: 201, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-fhb/complaint/show/:id', text: '投诉管理详情'},
  {id: 203, pid: 200, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-fhb/refund/index', text: '退款'},
  {id: 204, pid: 203, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-fhb/refund/show/:id', text: '退款详情'},
  {id: 210, pid: 0, icon: 'm-menu__link-icon iconfont icon-zhongguohangtiantubiaoheji-weizhuanlunkuo-', path: '/ss-fhb/product/manage', text: '我的产品'},

  {id: 400, pid: 0, icon: 'm-menu__link-icon iconfont icon-xiaoxizhongxin', path: '/ss-fhb/systemNotice', text: '消息中心'},
  {id: 500, pid: 0, icon: 'm-menu__link-icon iconfont icon-shujubaobiao', path: '/ss-fhb/report', text: '我的报表'},
  {
    id: 502,
    pid: 500,
    icon: 'fa fa-edit',
    path: '/ss-fhb/report/index/forms_market_01?gateway=ms-fahuobao-merchant-forms',
    text: '订单毛利报表'
  },

  {
    id: 1000,
    pid: 0,
    icon: 'm-menu__link-icon iconfont icon-yichangshijianliebiaocopy',
    path: './exception/#/ss-e/anomaly/query/index',
    text: '异常中心'
  },

  {id: 100000, pid: 100001, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-fhb', text: '占位的', hidden: 1}
]
