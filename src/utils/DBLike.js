class DBLike {
  constructor(data) {
    this.data = data
  }

  /**
   * 搜索
   * @param params obj
   * @returns array
   */
  search(params) {
    return this.data.filter(v => {
      for (let k in params) {
        if (params[k]) {
          if (v[k] && v[k].indexOf(params[k]) < 0) {
            return false
          }
        }
      }
      return true
    })
  }
}

export default DBLike
