// 倒计时
export const TimeReduce = class TimeReduce {
  cb

  startTime

  // 初始化
  constructor(times = 60, cb = () => {
  }) {
    this.startTime = times
    this.times = times
    this.cb = cb
  }

  // 时间递减
  reduce() {
    if (this.times > 0) {
      this.cb(this.times)
      this.times--
      setTimeout(() => {
        this.reduce()
      }, 1000)
    }
  }
}

export default TimeReduce
