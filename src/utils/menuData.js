import menuDataFhb from './menuDataFhb.js'
import menuDataOms from './menuDataOms.js'

export default process.env.projectType === 'fhbMant' ? menuDataFhb : menuDataOms
