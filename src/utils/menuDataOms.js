export default [

  // 下单 （总）
  {id: 1, pid: 0, icon: 'm-menu__link-icon iconfont icon-filedone', path: '/ss-oms/order', text: '下单'},
  {id: 2, pid: 1, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/order/create', text: '入库订单'},
  {id: 3, pid: 2, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/order/create/:id', text: '入库单-修改'},
  {id: '2-10-23', pid: 1, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/order/create-out', text: '出库订单'},
  // 入库暂存单
  {id: 4, pid: 1, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/order/temp', text: '入库暂存单'},
  {id: 5, pid: 4, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/order/temp-show/:id', text: '入库暂存单-详情'},

  // 入库暂存单
  {id: '4-1', pid: 1, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/order/temp-out', text: '出库暂存单'},
  {
    id: '5-1',
    pid: '4-1',
    icon: 'm-menu__link-icon flaticon-layers',
    path: '/ss-oms/order/temp-out-show/:id',
    text: '出库暂存单-详情'
  },

  {id: 6, pid: 1, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/order/create-service', text: '服务订单'},
  {id: 7, pid: 6, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/order/create-service/:id', text: '服务订单-修改'},

  // 服务暂存单
  {id: 8, pid: 1, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/order/temp-service', text: '服务暂存单'},
  {
    id: 9,
    pid: 8,
    icon: 'm-menu__link-icon flaticon-layers',
    path: '/ss-oms/order/temp-service-show/:id',
    text: '服务暂存单-详情'
  },
  // 预留：10、11整装工程单
  // 预留：12、13  14 15工程暂存单

  // 订单（总）
  {id: 20, pid: 0, icon: 'm-menu__link-icon iconfont icon-solution', path: '/ss-oms/orders', text: '订单'},
  {
    id: 21,
    pid: 20,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/orders/service-orders/index',
    text: '服务订单'
  },
  {
    id: 22,
    pid: 21,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/orders/service-orders/show/:id',
    text: '服务订单详情'
  },
  {
    id: '21-1',
    pid: 20,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/orders/service-orders/edit-index',
    text: '服务订单修改'
  },
  {
    id: '22-1',
    pid: '21-1',
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/orders/service-orders/edit-show/:id',
    text: '服务订单修改详情'
  },
  {
    id: '22-2',
    pid: '21-1',
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/orders/service-orders/edit/:id',
    text: '服务订单修改详情'
  },
  {
    id: '21-3',
    pid: 20,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/orders/service-orders/export',
    text: '服务订单导出'
  },
  {
    id: 23,
    pid: 20,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/orders/user-pick/index',
    text: '用户自提单'
  },
  {
    id: 24,
    pid: 20,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/orders/receivables/index',
    text: '代收款订单'
  },
  // 入库（总）
  {id: 25, pid: 0, icon: 'm-menu__link-icon iconfont icon-in-stroage', path: '/ss-oms/enterWarehouse', text: '入库'},
  // 入库单
  {
    id: 26,
    pid: 25,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehousing-order/index',
    text: '入库单'
  },
  {
    id: 27,
    pid: 26,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehousing-order/show/:id',
    text: '入库单-详情'
  },
  {
    id: '26-1',
    pid: 20,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehousing-order/edit-index',
    text: '入库订单修改'
  },
  {
    id: '26-2',
    pid: '26-1',
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehousing-order/edit-show/:id',
    text: '入库订单修改详情'
  },
  {
    id: '26-3',
    pid: '26-1',
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehousing-order/edit/:id',
    text: '入库订单修改详情'
  },

  // 入库库位分配
  {
    id: 28,
    pid: 25,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehouse-allocation/index',
    text: '入库库位分配'
  },
  {
    id: 29,
    pid: 28,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehouse-allocation/show/:id',
    text: '入库库位分配-详情'
  },
  {
    id: '29-1',
    pid: 28,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehousing-order/order-show/:id',
    text: '订单详情',
    hidden: '1'
  },

  // 入库库位分配CD
  {
    id: 30,
    pid: 25,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehouse-allocation-cd/index',
    text: '入库库位分配-CD'
  },
  {
    id: 31,
    pid: 30,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehouse-allocation-cd/show/:id',
    text: '入库库位分配CD-详情'
  },
  {
    id: '30-1',
    pid: 30,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehouse-allocation-cd/order-show/:id',
    text: '订单详情',
    hidden: '1'
  },

  // 入库拣货单
  {
    id: 32,
    pid: 25,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehouse-picklist/index',
    text: '入库拣货单'
  },
  {
    id: 33,
    pid: 32,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehouse-picklist/show/:id',
    text: '入库拣货单-详情'
  },

  // 入库确认
  {
    id: 34,
    pid: 25,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehouse-confirm/index',
    text: '入库确认'
  },
  {
    id: 35,
    pid: 34,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehouse-confirm/show/:id',
    text: '入库确认-详情'
  },
  {
    id: '34-1',
    pid: 34,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/enterWarehouse/warehouse-confirm/order-show/:id',
    text: '订单详情',
    hidden: '1'
  },

  // 到件管理
  {
    id: 36,
    pid: 25,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/orders/arrive-manager/index',
    text: '到件管理'
  },
  {
    id: 37,
    pid: 36,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/orders/arrive-manager/arrive/:id',
    text: '到件管理详情'
  },
  {
    id: '37-1',
    pid: 37,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/orders/arrive-manager/show/:id',
    text: '订单详情',
    hidden: '1'
  },
  // 预留 36~39
  // 出库（总）
  {id: 40, pid: 0, icon: 'm-menu__link-icon iconfont icon-upload', path: '/ss-oms/outputWarehouse', text: '出库'},

  // 出库单
  {
    id: 41,
    pid: 40,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/outputWarehouse/warehousing-order/index',
    text: '出库单'
  },
  {
    id: 42,
    pid: 41,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/outputWarehouse/warehousing-order/show/:id',
    text: '出库单-详情'
  },

  // 出库库位分配
  {
    id: 43,
    pid: 40,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/outputWarehouse/warehouse-allocation/index',
    text: '出库库位分配'
  },
  {
    id: 44,
    pid: 43,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/outputWarehouse/warehouse-allocation/show/:id',
    text: '出库库位分配-详情'
  },
  {
    id: '43-1',
    pid: 43,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/outputWarehouse/warehouse-allocation/order-detail/:id',
    text: '订单详情',
    hidden: '1'
  },

  // 出库库位分配CD
  {
    id: 45,
    pid: 40,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/outputWarehouse/warehouse-allocation-cd/index',
    text: '出库库位分配-CD'
  },
  {
    id: 46,
    pid: 45,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/outputWarehouse/warehouse-allocation-cd/show/:id',
    text: '出库库位分配-CD-详情'
  },
  {
    id: '45-1',
    pid: 45,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/outputWarehouse/warehouse-allocation-cd/order-detail/:id',
    text: '订单详情',
    hidden: '1'
  },

  // 出库拣货单
  {
    id: 47,
    pid: 40,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/outputWarehouse/warehouse-picklist/index',
    text: '出库拣货单'
  },
  {
    id: 48,
    pid: 47,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/outputWarehouse/warehouse-picklist/show/:id',
    text: '出库拣货单-详情'
  },

  // 出库确认
  {
    id: 49,
    pid: 40,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/outputWarehouse/warehouse-confirm/index',
    text: '出库确认'
  },
  {
    id: 50,
    pid: 49,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/outputWarehouse/warehouse-confirm/show/:id',
    text: '出库确认-详情'
  },
  {
    id: '49-1',
    pid: 49,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/outputWarehouse/warehouse-confirm/order-detail/:id',
    text: '订单详情',
    hidden: '1'
  },

  // 预约 (总)
  {id: 51, pid: 0, icon: 'm-menu__link-icon iconfont icon-reloadtime', path: '/ss-oms/appoint', text: '预约'},
  {
    id: 52,
    pid: 51,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/dispatch/distribution-order/index',
    text: '配装服务单预约'
  },
  {
    id: '52-1',
    pid: 52,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/dispatch/distribution-order/order-detail/:id',
    text: '订单详情',
    hidden: '1'
  },
  {id: 53, pid: 51, icon: 'm-menu__link-icon iconfont icon-upload', path: '/ss-oms/appoint/index', text: '用户自提预约'},
  {
    id: '53-1',
    pid: 53,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/appoint/order-detail/:id',
    text: '订单详情',
    hidden: '1'
  },
  // 预留54~59
  // 异常
  {id: 54, pid: 0, icon: 'm-menu__link-icon iconfont icon-carryout', path: '/ss-oms/abnormal', text: '异常'},
  {
    id: 200,
    pid: 54,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/abnormal/launch/index',
    text: '发起仓库异常'
  },
  {
    id: 202,
    pid: 54,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/abnormal/list/index',
    text: '仓库异常'
  },
  {
    id: 203,
    pid: 202,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/abnormal/list/show/:id',
    text: '仓库异常详情'
  },
  {
    id: 204,
    pid: 54,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/service/abnormal/index',
    text: '服务异常'
  },
  {
    id: 205,
    pid: 204,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/service/abnormal/show/:id',
    text: '服务异常详情'
  },
  // 调度（总）
  {id: 60, pid: 0, icon: 'm-menu__link-icon iconfont icon-tiaoduguanli', path: '/ss-oms/dispatch', text: '调度'},
  // 配装一体调度
  // {
  //   id: 61,
  //   pid: 60,
  //   icon: 'm-menu__link-icon iconfont icon-upload',
  //   path: '/ss-oms/dispatch/loading/index',
  //   text: '配装一体调度'
  // },
  // {
  //   id: '61-1',
  //   pid: 61,
  //   icon: 'm-menu__link-icon iconfont icon-upload',
  //   path: '/ss-oms/dispatch/loading/order-detail/:id',
  //   text: '订单详情',
  //   hidden: '1'
  // },
  // 配送调度
  // {
  //   id: 62,
  //   pid: 60,
  //   icon: 'm-menu__link-icon iconfont icon-upload',
  //   path: '/ss-oms/dispatch/distribution/index',
  //   text: '配送调度'
  // },
  // {
  //   id: '62-1',
  //   pid: 62,
  //   icon: 'm-menu__link-icon iconfont icon-upload',
  //   path: '/ss-oms/dispatch/distribution/order-detail/:id',
  //   text: '订单详情',
  //   hidden: '1'
  // },
  // 安装调度
  {
    id: 63,
    pid: 60,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/dispatch/install/index',
    text: '服务调度'
  },
  {
    id: '63-1',
    pid: 63,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/dispatch/install/order-detail/:id',
    text: '订单详情',
    hidden: '1'
  },
  // 提货调度
  {
    id: 64,
    pid: 60,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/dispatch/delivery/index',
    text: '提货调度'
  },
  {
    id: '64-1',
    pid: 64,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/dispatch/delivery/order-detail/:id',
    text: '订单详情',
    hidden: '1'
  },
  // 二级中转调度
  {
    id: 65,
    pid: 60,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/dispatch/two-grade-transfer-dispatch/index',
    text: '干线调度'
  },
  {
    id: '65-1',
    pid: 65,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/dispatch/two-grade-transfer-dispatch/order-detail/:id',
    text: '订单详情',
    hidden: '1'
  },
  // 预留65~69
  // 打印 (总)
  {id: 70, pid: 0, icon: 'm-menu__link-icon iconfont icon-printer', path: '/ss-oms/print', text: '打印'},
  {id: 71, pid: 70, icon: 'm-menu__link-icon iconfont icon-upload', path: '/ss-oms/print/label/index', text: '标签'},
  {id: 75, pid: 71, icon: 'm-menu__link-icon iconfont icon-upload', path: '/ss-oms/print/label/show/:id', text: '标签打印详情'},
  {
    id: 72,
    pid: 70,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/print/distribution/index',
    text: '配送装车单'
  },
  {id: 73, pid: 70, icon: 'm-menu__link-icon iconfont icon-upload', path: '/ss-oms/print/pick/index', text: '提货装车单'},
  {
    id: 74,
    pid: 70,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/print/two-grade-transfer/index',
    text: '二级中转装车单'
  },
  // 预留 74~79
  // 仓库管理（总菜单）
  {id: 80, pid: 0, icon: 'm-menu__link-icon iconfont icon-home', path: '/ss-oms/warehouse', text: '仓库管理'},
  {
    id: 81,
    pid: 80,
    icon: 'm-menu__link-icon iconfont icon-home',
    path: '/ss-oms/warehouse/configuration/index',
    text: '仓库初始化设置'
  },
  {
    id: 82,
    pid: 81,
    icon: 'm-menu__link-icon iconfont icon-home',
    path: '/ss-oms/warehouse/configuration/edit/:id',
    text: '库位设置'
  },
  {
    id: 83,
    pid: 80,
    icon: 'm-menu__link-icon iconfont icon-home',
    path: '/ss-oms/warehouse/transfer/index',
    text: '移库管理'
  },
  {
    id: 84,
    pid: 83,
    icon: 'm-menu__link-icon iconfont icon-home',
    path: '/ss-oms/warehouse/transfer/add',
    text: '新增移库单',
    hidden: 1
  },
  {
    id: 85,
    pid: 84,
    icon: 'm-menu__link-icon iconfont icon-home',
    path: '/ss-oms/warehouse/transfer/show/:id',
    text: '移库单详情'
  },
  {
    id: 200,
    pid: 80,
    icon: 'm-menu__link-icon iconfont icon-home',
    path: '/ss-oms/warehouse/inventory/index',
    text: '盘库列表'
  },
  {
    id: 201,
    pid: 200,
    icon: 'm-menu__link-icon iconfont icon-home',
    path: '/ss-oms/warehouse/inventory/add',
    text: '新增盘库单',
    hidden: '1'
  },
  {
    id: 202,
    pid: 200,
    icon: 'm-menu__link-icon iconfont icon-home',
    path: '/ss-oms/warehouse/inventory/register/:id',
    text: '登记盘库数据'
  },
  {
    id: 203,
    pid: 200,
    icon: 'm-menu__link-icon iconfont icon-home',
    path: '/ss-oms/warehouse/inventory/show/:id',
    text: '盘点详情'
  },
  // 报亏管理
  {
    id: 899,
    pid: 80,
    icon: 'm-menu__link-icon iconfont icon-home',
    path: '/ss-oms/warehouse/loss-report/launch',
    text: '发起报亏单'
  },
  {
    id: 900,
    pid: 80,
    icon: 'm-menu__link-icon iconfont icon-home',
    path: '/ss-oms/warehouse/loss-report/index',
    text: '报亏单列表'
  },
  {
    id: 901,
    pid: 900,
    icon: 'm-menu__link-icon iconfont icon-home',
    path: '/ss-oms/warehouse/loss-report/show/:id',
    text: '报亏单审核详情'
  },
  // 报溢管理
  {
    id: 910,
    pid: 80,
    icon: 'm-menu__link-icon iconfont icon-home',
    path: '/ss-oms/warehouse/overflow/launch',
    text: '发起报溢单'
  },
  {
    id: 911,
    pid: 80,
    icon: 'm-menu__link-icon iconfont icon-home',
    path: '/ss-oms/warehouse/overflow/index',
    text: '报溢单列表'
  },
  {
    id: 912,
    pid: 911,
    icon: 'm-menu__link-icon iconfont icon-home',
    path: '/ss-oms/warehouse/overflow/show/:id',
    text: '报溢单审核详情'
  },
  // 预留81-83：仓库设置
  // 预留84-86：库位设置
  // 商家长期库存
  {
    id: 87,
    pid: 80,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/warehouse/owner-longterm-storage/index',
    text: '商家长期库存'
  },
  {
    id: 88,
    pid: 87,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/warehouse/owner-longterm-storage/show/:id',
    text: '商家长期库存-详情'
  },
  {
    id: 89,
    pid: 88,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/warehouse/owner-longterm-storage/productDetail/:id/:ownerId',
    text: '商家长期库存-产品详情'
  },
  // 预留：整装工程库存
  // 临时仓库
  {
    id: 90,
    pid: 80,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/warehouse/temp-store/index',
    text: '商家临时库存'
  },
  {
    id: 91,
    pid: 90,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/warehouse/temp-store/show/:id',
    text: '商家产品包件列表'
  },
  {
    id: 92,
    pid: 91,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/warehouse/temp-store/productDetail/:id/:ownerId',
    text: '商家临时库存-产品详情'
  },
  // 服务资源（总）
  {id: 100, pid: 0, icon: 'm-menu__link-icon iconfont icon-car', path: '/ss-oms/resource', text: '服务资源'},
  {
    id: 103,
    pid: 100,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/resource/pickup/index',
    text: '提货资源管理'
  },
  {
    id: 104,
    pid: 100,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/resource/secondary/index',
    text: '干线资源管理'
  },
  {
    id: 105,
    pid: 100,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/resource/pick/index',
    text: '拣货资源管理'
  },
  {
    id: 106,
    pid: 100,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/resource/distributionInstall/index',
    text: '配安资源管理'
  },
  // 预留104~109
  // 基础资料(总)
  {id: 110, pid: 0, icon: 'm-menu__link-icon iconfont icon-container', path: '/ss-oms/basic-data', text: '基础资料'},
  {
    id: 111,
    pid: 110,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/company/index',
    text: '公司'
  },
  {
    id: 112,
    pid: 111,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/company/show/:id',
    text: '公司详情'
  },
  {
    id: '112-z-1',
    pid: 111,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/company/create',
    text: '公司新增',
    hidden: 1
  },
  {
    id: '112-z-2',
    pid: 111,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/company/edit/:id',
    text: '公司编辑'
  },

  {
    id: 113,
    pid: 110,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/company/my',
    text: '我的公司'
  },

  {
    id: 114,
    pid: 110,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/manufactor/index',
    text: '厂家基础资料'
  },
  {
    id: 115,
    pid: 114,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/manufactor/show/:id',
    text: '厂家详情'
  },
  {
    id: '115-z-1',
    pid: 114,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/manufactor/create',
    text: '厂家新增',
    hidden: 1
  },
  {
    id: '115-z-2',
    pid: 114,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/manufactor/edit/:id',
    text: '厂家编辑'
  },

  {
    id: 116,
    pid: 110,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/manufactor-pro/index',
    text: '厂家产品维护'
  },
  {
    id: 117,
    pid: 116,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/manufactor-pro/index-pro/:id',
    text: '厂家商品详情'
  },
  {
    id: '117-z-1',
    pid: 116,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/manufactor-pro/create',
    text: '厂家产品新增',
    hidden: 1
  },
  {
    id: '117-z-2',
    pid: 116,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/manufactor-pro/edit/:id',
    text: '厂家产品编辑'
  },

  {
    id: 118,
    pid: 110,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/owner/index',
    text: '商家基础资料'
  },
  {
    id: 119,
    pid: 118,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/owner/show/:id',
    text: '商家详情'
  },
  {
    id: '118-z-1',
    pid: 118,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/owner/create',
    text: '商家新增',
    hidden: 1
  },
  {
    id: '118-z-2',
    pid: 118,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/owner/edit/:id',
    text: '商家编辑'
  },

  {
    id: 120,
    pid: 110,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/production/index',
    text: '商家产品维护'
  },
  {
    id: 121,
    pid: 120,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/basic-data/production/show/:id',
    text: '商家产品详情'
  },
  // 账号管理(总)
  {id: 130, pid: 0, icon: 'm-menu__link-icon iconfont icon-user1', path: '/ss-oms/user', text: '账号管理'},
  {id: 131, pid: 130, icon: 'm-menu__link-icon iconfont icon-upload', path: '/ss-oms/user/my', text: '我的账号'},
  {id: 132, pid: 130, icon: 'm-menu__link-icon iconfont icon-upload', path: '/ss-oms/user/index', text: '账号列表'},
  {id: 133, pid: 132, icon: 'm-menu__link-icon iconfont icon-upload', path: '/ss-oms/user/show/:id', text: '账号详情'},
  {
    id: 134,
    pid: 133,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/user/create/:id',
    text: '账号修改',
    hidden: '1'
  },
  {
    id: 135,
    pid: 132,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/user/create',
    text: '账号新增',
    hidden: '1'
  },
  // 预留：我的账号
  // 预留：账号管理
  // 信息中心
  {id: 140, pid: 0, icon: 'm-menu__link-icon iconfont icon-xiaoxizhongxin', path: '/ss-oms/information', text: '信息中心'},
  {id: 141, pid: 140, icon: 'm-menu__link-icon iconfont icon-upload', path: '/ss-oms/information/menu', text: '菜单管理'},
  {id: 142, pid: 140, icon: 'm-menu__link-icon iconfont icon-upload', path: '/ss-oms/information/role', text: '角色管理'},
  {
    id: 143,
    pid: 142,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/information/role-show',
    text: '角色新增',
    hidden: '1'
  },
  {
    id: 144,
    pid: 142,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/information/role-show/:id',
    text: '角色详情'
  },
  {
    id: 145,
    pid: 140,
    icon: 'm-menu__link-icon flaticon-layers',
    path: '/ss-oms/information/cash-examine/index',
    text: '提现审核'
  },
  {
    id: 146,
    pid: 140,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/info/user/index',
    text: '账号列表(系统)'
  },
  {id: 147, pid: 146, icon: 'm-menu__link-icon iconfont icon-upload', path: '/ss-oms/info/user/show/:id', text: '账号详情'},
  {
    id: 148,
    pid: 147,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/info/user/create/:id',
    text: '账号修改',
    hidden: '1'
  },
  {
    id: 149,
    pid: 146,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/info/user/create',
    text: '账号新增',
    hidden: '1'
  },
  // 时效管理
  {id: 150, pid: 0, icon: 'm-menu__link-icon iconfont icon-reloadtime', path: '/ss-oms/orders/time-further', text: '时效管理'},
  {
    id: 151,
    pid: 150,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/orders/time-further/index',
    text: '时效跟进'
  },
  {
    id: 152,
    pid: 151,
    icon: 'm-menu__link-icon iconfont icon-upload',
    path: '/ss-oms/orders/time-further/show/:id',
    text: '时效跟进详情'
  },
  // 占位的

  {id: 160, pid: 0, icon: 'm-menu__link-icon iconfont icon-weibiaoti-', path: '/ss-oms/myWallet/index', text: '我的钱包'},
  {id: 161, pid: 160, icon: 'm-menu__link-icon iconfont icon-detail', path: '/ss-oms/myWallet/index', text: '我的钱包'},
  {id: 162, pid: 161, icon: 'fa fa-edit', path: '/ss-oms/myWallet/setPsdShow', text: '钱包密码', hidden: 1},
  {id: 163, pid: 161, icon: 'fa fa-edit', path: '/ss-oms/myWallet/bankCardIndex', text: '银行卡列表', hidden: 1},
  {id: 164, pid: 163, icon: 'fa fa-edit', path: '/ss-oms/myWallet/addBankCard', text: '添加银行卡', hidden: 1},
  {id: 165, pid: 161, icon: 'fa fa-edit', path: '/ss-oms/my-wallet/log-list', text: '账单流水列表', hidden: 1},
  // {id: 166, pid: 161, icon: 'fa fa-edit', path: '/ss-oms/myWallet/recharge/:money/:totalMoney', text: '钱包充值'},

  {id: 170, pid: 0, icon: 'fa fa-edit', path: '/ss-oms/resource/common/show/:id', text: '师傅详情', hidden: 1},
  // 账号管理(总)
  {id: 500, pid: 0, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/report', text: '报表管理'},
  {
    id: 502,
    pid: 500,
    icon: 'm-menu__link-icon iconfont icon-user',
    path: '/ss-oms/any-report/652',
    text: '服务对账报表'
  },
  {id: 300, pid: 0, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/orders/expatriate', text: '订单外派'},
  {id: 301, pid: 300, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/expatriateOrders/index', text: '抛出订单'},
  {id: 302, pid: 301, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/expatriateOrders/show/:id', text: '抛出订单-详情'},
  {id: 303, pid: 300, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/offering/index', text: '报价中'},
  {id: 304, pid: 303, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/offering/show/:id', text: '报价订单-详情'},
  {id: 305, pid: 300, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/pending/index', text: '待付款'},
  {id: 306, pid: 305, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/pending/show/:id', text: '待付款订单-详情'},
  {id: 307, pid: 300, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/inService/index', text: '服务中'},
  {id: 308, pid: 307, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/inService/show/:id', text: '服务中订单-详情'},
  {id: 309, pid: 300, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/confirming/index', text: '待确认'},
  {id: 310, pid: 309, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/confirming/show/:id', text: '已完成订单-详情'},
  {id: 311, pid: 300, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/completed/index', text: '已完成'},
  {id: 312, pid: 311, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/completed/show/:id', text: '已完成订单-详情'},
  {id: 313, pid: 303, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/offering/payment/:id', text: '报价支付'},
  {id: 313, pid: 303, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/pending/payment/:id', text: '待付款支付'},
  {id: 314, pid: 303, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/inService/paymentAdditional/:id', text: '服务中追加费用'},
  {id: 314, pid: 303, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/orders/expatriate/confirming/paymentAdditional/:id', text: '待确认追加费用'},
  {id: 330, pid: 80, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/warehouse/record/OutWarehouse', text: '出库记录'},
  {id: 331, pid: 330, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/warehouse/record/OutShow/:id', text: '出库记录详情'},
  {id: 332, pid: 80, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/warehouse/record/EnterWarehouse', text: '入库记录'},
  {id: 333, pid: 332, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/warehouse/record/EnterShow/:id', text: '入库记录详情'},

  {id: 350, pid: 0, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/shop', text: '小二家具商城'},
  {id: 351, pid: 350, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/shop/show', text: '商城基础资料'},
  {id: 352, pid: 350, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/shop/store', text: '体验门店管理'},
  {id: 354, pid: 350, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/shop/goods', text: '商品管理'},
  {
    id: 355,
    pid: 354,
    icon: 'm-menu__link-icon iconfont icon-linechart',
    path: '/ss-oms/shop/goods/create',
    text: '新增商品',
    hidden: 1
  },
  {
    id: 356,
    pid: 354,
    icon: 'm-menu__link-icon iconfont icon-linechart',
    path: '/ss-oms/shop/goods/edit/:id',
    text: '编辑商品'
  },
  {
    id: 357,
    pid: 350,
    icon: 'm-menu__link-icon iconfont icon-linechart',
    path: '/ss-oms/shop/trade',
    text: '交易管理'
  },
  {
    id: 358,
    pid: 357,
    icon: 'm-menu__link-icon iconfont icon-linechart',
    path: '/ss-oms/shop/delivery/:id',
    text: '确认发货'
  },
  {
    id: 359,
    pid: 350,
    icon: 'm-menu__link-icon iconfont icon-linechart',
    path: '/ss-oms/shop/tradecustomer',
    text: '交易管理(客服)'
  },
  {id: 400, pid: 0, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/trunkLine', text: '线路管理'},
  {id: 401, pid: 400, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/trunkLine/index', text: '线路管理'},
  {id: 402, pid: 401, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/trunkLine/show/:id', text: '线路详情'},
  {id: 403, pid: 402, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/trunkLine/edit', text: '线路编辑'},
  {id: 404, pid: 400, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/compensatePromise/index', text: '赔付承诺'},

  {id: 600, pid: 0, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/trunkOrder', text: '干线订单管理'},
  // {id: 601, pid: 600, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/trunkOrder/index', text: '订单查询'},
  // {id: 602, pid: 601, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/trunkOrder/show/:id', text: '订单详情'},
  {id: 603, pid: 600, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/trunkOrder/collect/index', text: '待揽收订单'},
  // {id: 604, pid: 603, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/trunkOrder/collect/show/:id', text: '待揽收订单详情'},

  {id: 800, pid: 0, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/approve', text: '审核管理'},
  {id: 801, pid: 800, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/approve/index', text: '资质审核'},
  {id: 802, pid: 801, icon: 'm-menu__link-icon iconfont icon-linechart', path: '/ss-oms/approve/detail/:id', text: '资质详情'},
  // 提货地址管理
  {id: 810, pid: 0, icon: 'm-menu__link-icon iconfont icon-dizhiguanli', path: '/ss-oms/pickAddress', text: '提货地址管理'},
  {id: 811, pid: 810, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-oms/pickAddress/index', text: '提货地址管理'},
  {
    id: 1000,
    pid: 0,
    icon: 'm-menu__link-icon iconfont icon-yichangshijianliebiaocopy',
    path: './exception/#/ss-e/anomaly/query/index',
    text: '异常中心'
  },
  {id: 100000, pid: 100001, icon: 'm-menu__link-icon flaticon-layers', path: '/ss-fhb-admin', text: '占位的'}
]
