import {httpGetResponse} from '@/utils/ajax'
import {dotData} from '@/utils'
import md5 from 'md5'

let options = {}
// 获取下拉框
const getOptions = (url, value = 'id', text = 'text', params = {}, dealResponse = (response) => {
  const data = dotData(response, 'data')
  let temp = []
  if (Array.isArray(data)) {
    data.forEach(v => {
      temp.push({
        text: v[text],
        value: v[value]
      })
    })
  }
  return temp
}) => {
  let uuid = md5(url + JSON.stringify(params))
  if (options[uuid]) {
    return Promise.resolve(options[uuid])
  }
  return httpGetResponse(url, params).then(response => {
    let temp = []
    const success = dotData(response, 'success')
    if (success) {
      temp = dealResponse(response)
    }
    if (temp.length > 0) {
      options[uuid] = temp
    }
    return temp
  })
}

// 获取所有库位
export function getPosition() {
  return getOptions('/ms-warehouse-order/storage-location/get-storage-location-list', 'key', 'value')
}

// 获取库位分配：入出库库位
export function getEnterWarehouse(params) {
  return getOptions('/ms-warehouse-order/storage-location/storage-location-list-order-id', 'key', 'value', params)
}

// 获取所有仓库列表
export function getAllWarehouse() {
  return getOptions('/ms-warehouse-order/WhPackage/findAllWarehouse', 'id', 'name')
}

// 获取仓库
export function getMyWarehouse() {
  return getOptions('/ms-warehouse-order/WhPackage/find-my-warehouse', 'id', 'name')
}

// 获取ERP--入库状态
export function getInputWarehouse() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/INPUTWAREHOUSE', 'value', 'text')
}

// 服务订单 货物来源
export function getCargoSourceType() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/zps-cargo-source-type', 'value', 'text')
}

// 服务订单 服务类型
export function getServiceType() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/zps-service-type', 'value', 'text')
}

// 服务订单 服务类型
export function getServiceTypeAdded() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/zps-service-type-added', 'value', 'text')
}

// 仓配装资源车辆类型
export function getCPZcarType() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/CPZCARTYPE', 'value', 'text')
}

// 安装业务类型
export function getInstallBusinessType() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/CPZBUSINESS', 'value', 'text')
}

// 安装服务类型
export function getInstallServiceType() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/CPZSERVICE', 'value', 'text')
}

// 增值服务
export function getAddedService() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/ch_added_service', 'value', 'text')
}

// 获取时间(上午，中午， 下午)
export function getTimeInterval() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/ch_time_interval', 'value', 'text')
}

// 出库状态
export function getOutWarehosueState() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/out_warehosue_state', 'value', 'text')
}

// 根据 ParentCode 获取字典 STORAGETYPE：存储类型 CHORDERSOURCE：订单来源
export function getSubDictListOfParentCode(code) {
  return getOptions('/ms-dictionary/dictionary/getSubDictListOfParentCode', 'value', 'text', {code})
}

// 获取带配置信息的服务类型
export function getAllServiceTypeByType(orderType) {
  return getOptions('/ms-warehouse-order/serviceType/findOrderNature', 'value', 'text', {orderType}, (response) => {
    return Array.isArray(response.data) ? response.data : []
  })
}

// 获取带配置信息的服务类型-结构调整后的数据
export function getAllServiceTypeByTypeTwo(orderType) {
  return getOptions('/ms-warehouse-order/serviceType/getServiceNature', 'value', 'text', {orderType}, (response) => {
    return response.data || {}
  })
}

// 职位
export function getDepartmentPosition() {
  return getOptions('/ms-user-storage/org-user/findByOrgDepartmentPosition', 'id', 'name')
}

// 订单状态
export function getOrderState() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/storageChOrderState', 'value', 'text')
}

// 发货宝-银行卡类型
export function getWalletBankXiaoer() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/walletBankXiaoer', 'value', 'text')
}

// 发货宝流水类型
export function getLsType() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/fhb-LS-type', 'value', 'text')
}

// 获取异常类型
export function getAbnormalType() {
  return getOptions('/ms-warehouse-order/orderAbnormal/getExceptionList', 'code', 'name')
}

// 用户系统
export function getUserProjectType() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/user-project-type', 'value', 'text')
}

// 代收款费用信息
export function getChCollectionCost() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/ch-collection-cost', 'value', 'text')
}

// 获取系统角色
export function getOrgRoleArr() {
  return getOptions('/ms-user-storage/org-user/get-org-role', 'id', 'name')
}

// 获取公司名和id
export function getCompanyName() {
  return getOptions('/ms-warehouse-order/basicData/findCompanyList', 'id', 'name')
}

// 获取公司账号
export function getAccounts() {
  return getOptions('/ms-warehouse-order/user/findStorageUserList', 'id', 'name', {queryStatus: '2'})
}

// 获取公司下的仓库
export function getWarehouse() {
  return getOptions('/ms-warehouse-order/ch-warehouse-manager/getWarehouseList', 'id', 'name', {queryStatus: '2'})
}

// 获取公司的业务模式
export function getCompanyBusinessType() {
  return getSubDictListOfParentCode('BUSINESSMODE')
}

// 文章管理文章栏目
export function getContentType(parentTypeNo = '') {
  return getOptions('/ms-content-data/content/getContentType', 'typeNo', 'type', {parentTypeNo}, (response) => {
    if (response.success) {
      if (Array.isArray(response.data)) {
        return response.data.map(v => {
          return {
            text: v.type,
            value: v.typeNo,
            pid: v.parentTypeNo
          }
        })
      }
    }
    return []
  })
}

// 文章管理所属产品
export function getFindContentList(parentTypeNo = '') {
  return getOptions('/ms-content-data/contentType/findContentList', 'typeNo', 'type')
}

// 发货宝运营-认证类型
export function getCredentialType() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/credential')
}

// 发货宝运营 师傅账号 服务类型 DJJFWLX
export function getServiceArr() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/DJJFWLX')
}

// 发货宝业务类型
export function getFhbBsteType() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/getFhbBsteType', 'code', 'name')
}

// 发货宝服务类型
export function getFhbServiceType() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/getFhbServiceType', 'code', 'name')
}

// 发货宝-业务类型
export function getAllBusinessTypes() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/businessType', 'value', 'text')
}

// 发货宝服订单来源
export function getFhbOrderSource() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/fhb-order-source', 'value', 'text')
}

// 投诉原因
export function getCOMPLAINCAUSE() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/COMPLAINCAUSE', 'value', 'text')
}

// 投诉性质
export function getFhbComplainNature() {
  return getOptions('/ms-dictionary/dictionaryInfoIndependent/fhbComplainNature')
}
// 发货宝获取指定的服务类型
export function getAppointServiceType(code) {
  return getOptions('/ms-fahuobao-order/FhbOrder/getServiceNoByBusiness', 'code', 'name', {
    pCode: code,
    systemCode: 'web'
  })
}

// oms 的业务类型
export function getOmsBusinessList() {
  return getOptions('/ms-warehouse-order/service-order-controller/get-business-list', 'code', 'name')
}

// oms 服务单可以的服务类型
export function getOmsCreateServiceList(businessNo, isStorage, storageType = '', orderType = 'service') {
  return getOptions('/ms-warehouse-order/service-order-controller/get-service-config', 'code', 'name', {
    businessNo,
    isStorage,
    storageType,
    orderType
  }, response => {
    if (Array.isArray(response.data)) {
      return response.data
    }
    return []
  })
}

// oms 根据业务类型和和服务类型展示模块
export function getOmsModuleIf(businessNo, serviceNos) {
  return getOptions('/ms-warehouse-order/service-order-controller/get-models-by-service', 'code', 'name', {
    businessNo,
    serviceNos: Array.isArray(serviceNos) ? serviceNos.join(',') : serviceNos
  }, response => {
    if (Array.isArray(response.data)) {
      return response.data
    }
    return []
  })
}

// 获取发货宝的支付方式
export function payFhbTypes() {
  return Promise.resolve([
    {value: '1', text: '支付宝', icon: '/static/images/faHuoBao/alipay.png', iconDis: '/static/images/faHuoBao/alipayDis.png'},
    {value: '2', text: '微　信', icon: '/static/images/faHuoBao/wechatpay.png', iconDis: '/static/images/faHuoBao/wechatpayDis.png'},
    // {value: 'fastPay', text: '快捷支付'},
    {value: '3', text: '钱包余额', icon: '/static/images/faHuoBao/balancepay.png', iconDis: '/static/images/faHuoBao/balancepayDis.png'}
  ])
}

// 获取直营的支付方式
export function payOmsTypes() {
  return Promise.resolve([
    {value: 'aliPay', text: '支付宝'},
    {value: 'weChatPay', text: '微　信'},
    {value: '3', text: '钱包余额'}
  ])
}

// 获取调度服务状态
export function getDispatchServiceState() {
  return Promise.resolve([
    {value: 1, text: '待出发'},
    {value: 2, text: '待上门'},
    {value: 3, text: '待签收'},
    {value: 4, text: '已完成'}])
}

// 获取包件单位
export function getPackageUnit() {
  return getSubDictListOfParentCode('PACKUNIT')
}
