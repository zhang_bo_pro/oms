import BasicTable from '@/components/BasicTable'
import CascadeAddress from '@/components/CascadeAddress'
import Dialog from '@/components/Dialog'
import Viewer from '@/components/Viewer'
import UploadImage from '@/components/Upload/UploadImage'
import Truncate from '@/components/Truncate'
import TreeSelect from '@/components/TreeSelect'
import Portlet from '@/components/Portlet'
import RemoteSelect from '@/components/RemoteSelect'
import Pagination from '@/components/Pagination'
import ProductSelect from '@/components/ProductSelect'
// 全局组件
export default {
  BasicTable,
  CascadeAddress,
  Dialog,
  Viewer,
  UploadImage,
  Truncate,
  TreeSelect,
  Portlet,
  RemoteSelect,
  Pagination,
  ProductSelect
}
