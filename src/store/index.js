import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import errorLog from './modules/errorLog'
import permission from './modules/permission'
import tagsView from './modules/tagsView'
import user from './modules/user'
import address from './modules/address.js'
import getters from './getters'
import menu from './modules/menu'
import recharge from './modules/recharge'
import productType from './modules/productType'
import configValue from './modules/configValue'
import notice from './modules/notice'
import im from './modules/im'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    errorLog,
    permission,
    tagsView,
    user,
    address,
    menu,
    productType,
    recharge,
    configValue,
    notice,
    im
  },
  getters
})

export default store
