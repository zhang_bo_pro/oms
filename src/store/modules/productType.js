import {copy} from '@/utils'
import {httpGetResponse} from '@/utils/ajax'
//  产品类型
const productType = {
  state: {
    store: {}
  },
  mutations: {
    setProductType(state, data) {
      state.store[data.code] = data.data
    }
  },
  actions: {
    getProductType({state, dispatch, commit}, code = '0') {
      if (state.store[code]) {
        return Promise.resolve(copy(state.store[code], true))
      } else {
        let params = {code}
        if (code === '0') {
          params.code = ''
        }
        return httpGetResponse('/ms-fahuobao-order/FhbOrder/getProductInfo', params).then(response => {
          if (response.success) {
            let empty = []
            if (Array.isArray(response.data)) {
              response.data.forEach(v => {
                let item = {}
                item.text = v.name
                item.value = v.code
                empty.push(item)
              })
              commit('setProductType', {data: empty, code})
              return empty
            }
          }
          return []
        })
      }
    }
  }
}

export default productType
