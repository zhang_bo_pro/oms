// 消息中心
const notice = {
  /*
  *  '1': '小二客服', '2': '订单助手' , '3', text: '平台消息' 4：'im消息'
  * */
  state: {
    type: '1',
    typeArr: [
      {value: '1', text: '小二客服', image: '/static/images/message-customer.png'},
      {value: '2', text: '订单助手', image: '/static/images/orderHelp.png'},
      {value: '3', text: '平台消息', image: '/static/images/message-system.png'}
    ]
  },
  mutations: {
    SET_NOTICE_TYPE: (state, type) => { // 设置消息类型
      state.type = type
    }
  },
  actions: {
    SET_NOTICE_TYPE({commit, state}, type) { // 设置消息类型
      commit('SET_NOTICE_TYPE', type)
    }
  },
  getters: {
    GET_NOTICE_TYPE: state => {
      return state.type
    },
    GET_NOTICE_TYPE_ARR: state => {
      return state.typeArr
    }
  }
}

export default notice
