// 读取底层配置项globalConfig
import Vue from 'vue'
const GLOBAL_CONFIG = {
  state: {
    config: {}
  },
  mutations: { // 修改状态
    SET_GLOBAL_CONFIG(state, {key, value}) {
      Vue.set(state.config, key, value)
    }
  },
  actions: { // 调用方法（接收传进来的参数）
    SET_GLOBAL_CONFIG({commit}, {key, value}) {
      commit('SET_GLOBAL_CONFIG', {key, value})
    }
  },
  getters: { // 获取最新状态
    GET_GLOBAL_CONFIG: state => (key) => {
      return state.config[key] || false
    }
  }
}
export default GLOBAL_CONFIG
