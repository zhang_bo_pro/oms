import {copy} from '@/utils'
import {httpGetResponse} from '@/utils/ajax'
const address = {
  state: {
    store: {}
  },
  mutations: {
    setStore(state, data) {
      state.store[data.value] = data.data
    }
  },
  actions: {
    getAddressData({state, dispatch, commit}, {value = 10000}) {
      if (state.store[value]) {
        return Promise.resolve(copy(state.store[value], true))
      } else {
        // parent_area_no
        return httpGetResponse('/ms-address/address/findByParentNo', {parentNo: value}).then((response) => {
          const success = response.success
          if (success && Array.isArray(response.data)) {
            let empty = []
            response.data.forEach((v) => {
              let item = {}
              item.text = v.areaName
              item.value = v.areaNo
              empty.push(item)
            })
            commit('setStore', {value, data: empty})
            return empty
          }
          return []
        })
      }
    }
  }
}

export default address
