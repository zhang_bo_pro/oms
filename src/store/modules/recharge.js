// 充值监听
import Vue from 'vue'
const rechargeResult = {
  state: {
    recharge: {}
  },
  mutations: { // 修改状态
    setRechargeResult(state, {key, result}) {
      Vue.set(state.recharge, key, result)
    }
  },
  actions: { // 调用方法（接收传进来的参数）
    setRechargeResult({commit}, {key, result}) {
      commit('setRechargeResult', {key, result})
    }
  },
  getters: { // 获取最新状态
    getRechargeResult: state => (key = '') => {
      if (key) {
        return state.recharge[key] || false
      }
      return false
    }
  }
}
export default rechargeResult
