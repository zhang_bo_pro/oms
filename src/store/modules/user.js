import {loginByUsername, logout} from '@/api/login'
import {getToken, removeToken, setToken, setUserInfo} from '@/utils/auth'
import {getLoginUser} from '@/utils/ajax'
import {dotData} from '@/utils'

const user = {
  state: {
    user: '',
    status: '',
    code: '',
    token: getToken(),
    name: '',
    avatar: '',
    introduction: '',
    roles: [],
    setting: {
      articlePlatform: []
    },
    userInfo: {} // 用户的基本信息
  },

  mutations: {
    SET_CODE: (state, code) => {
      state.code = code
    },
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_INTRODUCTION: (state, introduction) => {
      state.introduction = introduction
    },
    SET_SETTING: (state, setting) => {
      state.setting = setting
    },
    SET_STATUS: (state, status) => {
      state.status = status
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_USER_INFO: (state, userInfo) => {
      state.userInfo = userInfo
    }
  },

  actions: {
    // 用户名登录
    LoginByUsername({commit}, userInfo) {
      const username = userInfo.username.trim()
      return new Promise((resolve, reject) => {
        loginByUsername(username, userInfo.password).then(response => {
          const data = response.data
          commit('SET_TOKEN', data.token)
          setToken(response.data.token)
          resolve(true)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 登陆
    LoginUser({commit}, userInfo) {
      return new Promise((resolve, reject) => {
        const token = dotData(userInfo, 'token')
        const user = {
          name: dotData(userInfo, 'user.name') || dotData(userInfo, 'user.phone') || '未取得',
          id: dotData(userInfo, 'user.id')
        }
        setUserInfo(user)
        if (token) {
          setToken(token)
          resolve({token})
        } else {
          reject(new Error('token未获得'))
        }
      })
    },

    // 获取用户信息
    SET_USER_INFO({commit}, userInfo) {
      commit('SET_USER_INFO', userInfo)
    },

    // 第三方验证登录
    // LoginByThirdparty({ commit, state }, code) {
    //   return new Promise((resolve, reject) => {
    //     commit('SET_CODE', code)
    //     loginByThirdparty(state.status, state.email, state.code).then(response => {
    //       commit('SET_TOKEN', response.data.token)
    //       setToken(response.data.token)
    //       resolve()
    //     }).catch(error => {
    //       reject(error)
    //     })
    //   })
    // },

    // 登出
    LogOut({commit, state}) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          commit('SET_TOKEN', '')
          commit('SET_ROLES', [])
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    FedLogOut({commit}) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    },

    // 动态修改权限
    ChangeRoles({commit}, role) {
      return new Promise(resolve => {
        commit('SET_TOKEN', role)
        setToken(role)
        let user = getLoginUser()
        commit('SET_ROLES', ['admin'])
        commit('SET_NAME', user.user.name)
        commit('SET_AVATAR', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif')
        commit('SET_INTRODUCTION', '测试测试')
        resolve(user)
      })
    }
  },

  getters: {
    GET_USER_INFO: (state, getters) => (key) => {
      let userInfo = state.userInfo
      if (Object.values(userInfo).filter(v => !!v).length === 0) {
        userInfo = getLoginUser()
      }
      if (key) {
        return dotData(userInfo, key)
      }
      return userInfo
    }
  }
}

export default user
