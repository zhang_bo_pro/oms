import {copy, arrayToTree} from '@/utils'
const getters = {
  sidebar: state => state.app.sidebar,
  language: state => state.app.language,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  iFrameViews: state => state.tagsView.iFrameViews,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  introduction: state => state.user.introduction,
  status: state => state.user.status,
  roles: state => state.user.roles,
  setting: state => state.user.setting,
  permission_routers: state => state.permission.routers,
  addRouters: state => state.permission.addRouters,
  errorLogs: state => state.errorLog.logs,
  // GET_MENU_LIST isInit
  IS_MENU_LIST_INIT: state => state.menu.init,
  // 菜单列表
  GET_MENU_LIST: state => state.menu.list,
  // 树型菜单列表 过滤了隐藏
  GET_MENU_LIST_TREE: (state, getters) => {
    return arrayToTree(copy(getters.GET_MENU_LIST, true).filter(v => !/:/.test(v.path) && !v.hidden), 'id', 'pid', 'children', '0', true)
  },
  // 根据路由地址获取菜单的对象
  GET_MENU_LIST_MAP: state => (path) => {
    return state.menu.listMap[path]
  }
}
export default getters
