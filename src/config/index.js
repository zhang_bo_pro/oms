// 用户登录相关配装
export const APP_NAME = process.env.APP_NAME

// 登陆
export const loginUserKey = 'loginUser'

// 提交token
export const submitToken = 'submitToken'

// 菜单id设置
export const backendMenuId = 'backendMenuId'

// 后端代理前缀
export const urlPrefix = '/api'

export const interfaceArr = []

export const dataServiceConfig = {
  urlPrefix: urlPrefix
}
// 权限key
export const operationListKey = APP_NAME === 'FHB' ? 'operationListFHB' : 'operationListFHBAdmin'

// 默认全导出
export default {
  loginUserKey,
  backendMenuId,
  submitToken,
  dataServiceConfig
}
