import router from './router'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css'// progress bar style
import { getToken } from '@/utils/ajax' // getToken from cookie
import store from './store'

NProgress.configure({ showSpinner: false })// NProgress Configuration

const whiteList = ['/login', '/auth-redirect', '/test', '/register', '/edit-company', '/examining']// no redirect whitelist
let i = 1
router.beforeEach((to, from, next) => {
  NProgress.start() // start progress bar
  if (getToken()) { // determine if there has token
    /* has token*/
    if (to.path === '/login') {
      next({ path: '/' })
      NProgress.done()
    } else {
      // router.addRoutes(store.getters.addRouters) 添加菜单数据
      if (!store.getters.IS_MENU_LIST_INIT) { // 判断当前用户是否已拉取完user_info信息
        store.dispatch('INIT_MENU_LIST').then(_ => {
          store.dispatch('GenerateRoutes', {listMap: store.getters.GET_MENU_LIST_MAP}).then(() => { // 根据roles权限生成可访问的路由表
            router.addRoutes(store.getters.addRouters) // 动态添加可访问路由表
            if (i < 5) {
              setTimeout(_ => {
                i++
                next({...to, replace: true}) // hack方法 确保addRoutes已完成 ,set the replace: true so the navigation will not leave a history record
              }, 1000)
            } else {
              throw Error('初始菜单用户超时')
            }
          }).catch(e => {
            NProgress.done()
            console.error(e)
          })
        }).catch(e => {
          NProgress.done()
          console.error(e)
        })
      } else {
        // 没有动态改变权限的需求可直接next() 删除下方权限判断 ↓
        let flag = true
        if (flag) { // 判断能否进入
          next()
        } else {
          next({
            path: '/401',
            replace: true,
            query: { noGoBack: true }
          })
        }
      }
    }
  } else {
    /* has no token*/
    if (whiteList.indexOf(to.path) !== -1) { // 在免登录白名单，直接进入
      next()
    } else {
      next(`/login?redirect=${to.path}`) // 否则全部重定向到登录页
      NProgress.done() // if current page is login will not trigger afterEach hook, so manually handle it
    }
  }
})

router.afterEach(() => {
  NProgress.done() // finish progress bar
})
