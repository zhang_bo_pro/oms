const internalIp = require('internal-ip');
const config = require('./config/index.js')
require('dotenv').config()
const apiUrl = process.env.apiUrl || internalIp.v4.sync() + ':8763'
module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
    {
      name: 'OMS',
      script: 'index.js',
      cwd: config.build.productionAssetsRoot,
      env: {
        COMMON_VARIABLE: 'true'
      },
      env_production: {
        NODE_ENV: 'production'
      },
      args: `-p 3105 -u http://${apiUrl}`
    },
    {
      name: 'FHB-NEW',
      script: 'index.js',
      cwd: config.build.productionAssetsRootFhb,
      env: {
        COMMON_VARIABLE: 'true'
      },
      env_production: {
        NODE_ENV: 'production'
      },
      args: `-p 3102 -u http://${apiUrl}`
    }
  ]
};
